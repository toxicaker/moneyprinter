package org.toxicaker.jobs;

import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.stubbing.Answer;
import org.toxicaker.dal.mongo.CompanyDao;
import org.toxicaker.models.entities.CompanyProfileEntity;
import org.toxicaker.models.services.CompanyProfile;
import org.toxicaker.models.services.Symbol;
import org.toxicaker.services.FmpService;
import org.toxicaker.utils.DataConvertUtil;

class SyncCompanyProfileJobTest {

  @Mock
  private CompanyDao companyDao;
  @Mock
  private FmpService fmpService;

  private final Map<String, CompanyProfileEntity> db = new HashMap<>();

  private SyncCompanyProfileJob syncCompanyProfileJob;

  @BeforeEach
  void setUp() throws Exception {
    MockitoAnnotations.openMocks(this);
    var symbols = new ArrayList<Symbol>();
    symbols.add(Symbol.builder().symbol("MSFT").build());
    symbols.add(Symbol.builder().symbol("ORCL").build());
    var msftProfile = CompanyProfile.builder().symbol("MSFT").price(123.1).build();
    var orclProfile = CompanyProfile.builder().symbol("ORCL").price(21.3).build();
    when(fmpService.listSymbols()).thenReturn(symbols);
    when(fmpService.listCompanyProfiles(List.of("MSFT", "ORCL")))
        .thenReturn(List.of(msftProfile, orclProfile));
    when(companyDao.saveOrUpdateCompanyProfileEntity(argThat(companyProfileEntity ->
        companyProfileEntity != null && "MSFT".equals(companyProfileEntity.getSymbol()))))
        .thenAnswer((Answer<String>) invocationOnMock -> {
          db.put("MSFT", DataConvertUtil.toCompanyProfileEntity(msftProfile));
          return "MSFT";
        });
    when(companyDao.saveOrUpdateCompanyProfileEntity(argThat(companyProfileEntity ->
        companyProfileEntity != null && "ORCL".equals(companyProfileEntity.getSymbol()))))
        .thenAnswer((Answer<String>) invocationOnMock -> {
          db.put("ORCL", DataConvertUtil.toCompanyProfileEntity(orclProfile));
          return "ORCL";
        });
    syncCompanyProfileJob = new SyncCompanyProfileJob(companyDao, fmpService);
  }

  @AfterEach
  void tearDown() {
    db.clear();
  }

  @Test
  void testSaveCompanyEntityIfNotExists() throws Exception {
    syncCompanyProfileJob.execute();
    Assertions.assertNotNull(db.get("MSFT"));
    Assertions.assertNotNull(db.get("ORCL"));
    Assertions.assertEquals(123.1, db.get("MSFT").getPrice());
    Assertions.assertEquals(21.3, db.get("ORCL").getPrice());
  }

  @Test
  void testUpdateCompanyEntityIfExists() throws Exception {
    syncCompanyProfileJob.execute();
    Assertions.assertNotNull(db.get("MSFT"));
    Assertions.assertNotNull(db.get("ORCL"));
    Assertions.assertEquals(123.1, db.get("MSFT").getPrice());
    Assertions.assertEquals(21.3, db.get("ORCL").getPrice());
    var msftProfile = CompanyProfile.builder().symbol("MSFT").price(111.1).build();
    var orclProfile = CompanyProfile.builder().symbol("ORCL").price(21.44).build();
    when(fmpService.listCompanyProfiles(List.of("MSFT", "ORCL")))
        .thenReturn(List.of(msftProfile, orclProfile));
    when(companyDao.saveOrUpdateCompanyProfileEntity(argThat(companyProfileEntity ->
        companyProfileEntity != null && "MSFT".equals(companyProfileEntity.getSymbol()))))
        .thenAnswer((Answer<String>) invocationOnMock -> {
          db.put("MSFT", DataConvertUtil.toCompanyProfileEntity(msftProfile));
          return "MSFT";
        });
    when(companyDao.saveOrUpdateCompanyProfileEntity(argThat(companyProfileEntity ->
        companyProfileEntity != null && "ORCL".equals(companyProfileEntity.getSymbol()))))
        .thenAnswer((Answer<String>) invocationOnMock -> {
          db.put("ORCL", DataConvertUtil.toCompanyProfileEntity(orclProfile));
          return "ORCL";
        });
    syncCompanyProfileJob.execute();
    Assertions.assertNotNull(db.get("MSFT"));
    Assertions.assertNotNull(db.get("ORCL"));
    Assertions.assertEquals(111.1, db.get("MSFT").getPrice());
    Assertions.assertEquals(21.44, db.get("ORCL").getPrice());
  }
}