package org.toxicaker.jobs;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import com.sun.mail.smtp.SMTPMessage;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.mail.Message;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.stubbing.Answer;
import org.toxicaker.common.Constant;
import org.toxicaker.dal.mongo.ArkEntityDao;
import org.toxicaker.models.entities.ArkTradeEntity;
import org.toxicaker.services.impl.GmailServiceImpl;

class SyncARKTradesJobTest {

  @Mock
  private GmailServiceImpl gmailService;
  @Mock
  private ArkEntityDao arkEntityDao;
  @Mock
  private Message message;

  private SyncARKTradesJob syncARKTradesJob;

  private final Map<String, ArkTradeEntity> db = new HashMap<>();

  private static final String HTML =
      "<p style=\"font-family: Arial; font-size:13px; width:400px\">ARK offers fully\n"
          + "transparent Exchange Traded Funds (\"ETFs\") and provides investors with\n"
          + "trade information for all actively managed ETFs. This email only reflects\n"
          + "portfolio adjustments made by the ARK investment team. Files of trades are\n"
          + "not comprehensive lists of a day&#39;s trades for the ARK ETFs and\n"
          + "<strong><u>exclude</u></strong> initial/secondary public offering\n"
          + "transactions and ETF Creation/Redemption Unit activity. Complete holding\n"
          + "files are posted daily on <a href=\"https://u4959697.ct.sendgrid.net/ls/click?upn=zcd6lV4HLKVOGeJ9ek2kScEufCmUz1I3KeYPxo1NBJs-3Dndf3_STQdgZzPbPnsbGBfsl6bISX3yJ6pptpG33NGbnYX5HsOAWghtIpBQA4SlDnUrr8h-2FfDulSoagnSF0fOnQ9MLq0M6zXry7P84dHdWgYS4zNUyh0k68g630OOp2aJcZHyz3uvONDdNXRPh-2BRsNzoDqd-2FbRTsyCycd86gGR25kbxMziIY6IwksfqWokTPYFWvWdgHc6aPgB5KdiAShMKxcUIuTbTwMa5rlPktmX7H1qEgFF-2BLbQkB-2Fx-2FgQPW3KML5xtCnDt4kip6iQczudKfHkpOHI15sbveJjLINSKq2Xbftw-3D\"\n"
          + "target=\"_blank\">ark-funds.com</a>.</p><h4 style='margin-bottom:3px;\n"
          + "font-family: Arial;'>Latest Trades</h4><table border='0' cellspacing='0'\n"
          + "cellpadding='5' style='font-family: Arial; font-size:13px;'>\n"
          + "\t\t<tr bgcolor='#000' style='color:#fff; background-color:#000'>\n"
          + "\t\t\t<td></td>\n"
          + "\t\t\t<td>Fund</td>\n"
          + "\t\t\t<td>Date</td>\n"
          + "\t\t\t<td>Direction</td>\n"
          + "\t\t\t<td>Ticker</td>\n"
          + "\t\t\t<td>CUSIP</td>\n"
          + "\t\t\t<td>Company</td>\n"
          + "\t\t\t<td align='right'>Shares</td>\n"
          + "\t\t\t<td>% of ETF</td>\n"
          + "\t\t</tr><tr\n"
          + "style='background-color:#fff'><td>1</td><td>ARKG</td><td>03/22/2021</td><td>Buy</td><td>ACCD</td><td>00437E102</td><td>ACCOLADE\n"
          + "INC</td><td align='right'>2,626</td><td align='right'>0.0011</td></tr><tr\n"
          + "style='background-color:#EEE'><td>2</td><td>ARKG</td><td>03/22/2021</td><td>Buy</td><td>CDXS</td><td>192005106</td><td>CODEXIS\n"
          + "INC</td><td align='right'>44,373</td><td align='right'>0.0099</td></tr><tr\n"
          + "style='background-color:#fff'><td>3</td><td>ARKG</td><td>03/22/2021</td><td>Buy</td><td>IONS</td><td>462222100</td><td>IONIS\n"
          + "PHARMACEUTICALS INC</td><td align='right'>46,225</td><td\n"
          + "align='right'>0.0254</td></tr><tr\n"
          + "style='background-color:#EEE'><td>4</td><td>ARKG</td><td>03/22/2021</td><td>Sell</td><td>PSTI</td><td>72940R300</td><td>PLURISTEM\n"
          + "THERAPEUTICS INC</td><td align='right'>380</td><td\n"
          + "align='right'>0.0000</td></tr><tr\n"
          + "style='background-color:#fff'><td>5</td><td>ARKQ</td><td>03/22/2021</td><td>Buy</td><td>DDD</td><td>88554D205</td><td>3D\n"
          + "SYSTEMS CORP</td><td align='right'>141,254</td><td\n"
          + "align='right'>0.1205</td></tr><tr\n"
          + "style='background-color:#EEE'><td>6</td><td>ARKQ</td><td>03/22/2021</td><td>Buy</td><td>U</td><td>91332U101</td><td>UNITY\n"
          + "SOFTWARE INC</td><td align='right'>187,300</td><td\n"
          + "align='right'>0.5510</td></tr><tr\n"
          + "style='background-color:#fff'><td>7</td><td>ARKQ</td><td>03/22/2021</td><td>Buy</td><td>AONE</td><td>G7000X105</td><td>ONE</td><td\n"
          + "align='right'>17,570</td><td align='right'>0.0063</td></tr><tr\n"
          + "style='background-color:#EEE'><td>8</td><td>ARKQ</td><td>03/22/2021</td><td>Sell</td><td>BYDDY</td><td>05606L100</td><td>BYD\n"
          + "CO LTD</td><td align='right'>26,200</td><td\n"
          + "align='right'>0.0373</td></tr><tr\n"
          + "style='background-color:#fff'><td>9</td><td>ARKQ</td><td>03/22/2021</td><td>Sell</td><td>DE</td><td>244199105</td><td>DEERE\n"
          + "& CO</td><td align='right'>45,982</td><td align='right'>0.5004</td></tr><tr\n"
          + "style='background-color:#EEE'><td>10</td><td>ARKQ</td><td>03/22/2021</td><td>Sell</td><td>KMTUY</td><td>500458401</td><td>KOMATSU\n"
          + "LTD</td><td align='right'>12,700</td><td align='right'>0.0116</td></tr><tr\n"
          + "style='background-color:#fff'><td>11</td><td>ARKQ</td><td>03/22/2021</td><td>Sell</td><td>AVAV</td><td>008073108</td><td>AEROVIRONMENT\n"
          + "INC</td><td align='right'>8,096</td><td\n"
          + "align='right'>0.0282</td></tr></table><br><br/><a style=\"font-family:Arial;\n"
          + "font-size:13px;\" href=\"https://u4959697.ct.sendgrid.net/ls/click?upn=zcd6lV4HLKVOGeJ9ek2kSRXRFFQn1rBhvTyMa9aIC2TSfz2mEl5lAXMHUHfvzFMGifv3-2F7KZ8I7bEEsHpYElTzCpslUngm7Y-2Bujb6ctTE94XleBLRHLICULBkXGpVGUnwxXm_STQdgZzPbPnsbGBfsl6bISX3yJ6pptpG33NGbnYX5HsOAWghtIpBQA4SlDnUrr8hEfqY2gV8o7c-2BqQno3w-2FAdKNK2-2BuDl-2B8-2B7dJWObhcKjJ8gEbegsYtMT29BGK6Q98I23jUccezlgsNWcD689oukQ-2BbNqCZfuH4unCg1XiTZ9-2BPxue-2BSYYYruqBY4NPL9bx64tnWaheAL8otpZ2WM5fowDDUtIIi0bwMfhFIqgfsV3nCLRBrZirS4RzlRMNAtfUEMD9zZjH73jKJpd9x2x6KLVpowrzdG0qEpN1D6eX3s8-3D\">Download today&#39;s trades in\n"
          + "Excel format</a><br/><br/><br/><p style=\"font-family: Arial;\n"
          + "font-size:13px; width:400px\"><strong>*DISCLOSURES:</strong> Trade\n"
          + "notifications are for informational purposes only. ARK offers fully\n"
          + "transparent ETFs and provides trade information for all actively managed\n"
          + "ETFs. ARK&#39;s statements are not an endorsement of any company or a\n"
          + "recommendation to buy, sell or hold any security. Trade notification files\n"
          + "are not provided until full trade execution at the end of a trading day.\n"
          + "ARK may not trade every day. The time stamp of the email is the time of\n"
          + "file upload and not necessarily the exact time of the trades. Files of\n"
          + "trades are not comprehensive lists of a day&#39;s trades for the ARK ETFs\n"
          + "and exclude initial/secondary public offering transactions and ETF\n"
          + "Creation/Redemption Unit activity. Additional files may be posted at a\n"
          + "later time. Most ARK ETF portfolio trades settle on a T+2 basis. These\n"
          + "files represent an UNOFFICIAL, UNRECONCILED account, as all official\n"
          + "accounting and custody processes for the ARK ETFs are performed by BNY\n"
          + "Mellon resulting in a daily Net Asset Value (\"NAV\") for the ARK ETFs.\n"
          + "Updated ARK ETF end of day holdings are available on <a\n"
          + "href=\"https://u4959697.ct.sendgrid.net/ls/click?upn=zcd6lV4HLKVOGeJ9ek2kSWlYB09MKpc1FMvl4uUyen8ygJEzUJd3hvLRAU0QF8R5CTjh_STQdgZzPbPnsbGBfsl6bISX3yJ6pptpG33NGbnYX5HsOAWghtIpBQA4SlDnUrr8htA2Tim-2FI5Dis8shYVqcXVieQr7O6Fge-2FiEETumNnWAnKjmzZeaX3HCiK6YEj7Mlf33pUxr-2BwdxnnS-2F3HcxF62RC0OkVoFm7E-2FNVrCPnaO9RLc3NIOje0IqPn3H2058Pjxz5qmP46BRl6M1CBiOd1qFzIA6ksTv0jcLtMBr0bOxvnS2Rb7fraAJ2-2BSHY3PCLPhhM4WyQdkgadww5kyDCFWpW7MW-2FyrVotb0Eopuo-2B0vM-3D\"\n"
          + "target=\"_blank\">ark-funds.com/investor-resources</a></p><p\n"
          + "style=\"font-family: Arial; font-size:13px; width:400px\">The contents of\n"
          + "this email and any corresponding attachment/s are for informational\n"
          + "purposes only and is subject to change without notice. This email does not\n"
          + "constitute, either explicitly or implicitly, any provision of services or\n"
          + "products by ARK and investors are encouraged to consult counsel and/or\n"
          + "other investment professionals as to whether a particular investment\n"
          + "management service is suitable for their investment needs. All statements\n"
          + "made regarding companies or securities are strictly beliefs and points of\n"
          + "view held by ARK and are not endorsements by ARK of any company or security\n"
          + "or recommendations by ARK to buy, sell or hold any security. Historical\n"
          + "results are not indications of future results. Certain of the statements\n"
          + "contained in this content may be statements of future expectations and\n"
          + "other forward-looking statements that are based on ARK&#39;s current views\n"
          + "and assumptions and involve known and unknown risks and uncertainties that\n"
          + "could cause actual results, performance or events to differ materially from\n"
          + "those expressed or implied in such statements. The matters discussed via\n"
          + "this content may also involve risks and uncertainties described from time\n"
          + "to time in ARK&#39;s filings with the U.S. Securities and Exchange\n"
          + "Commission. ARK assumes no obligation to update any forward-looking\n"
          + "information contained in this content. Certain information was obtained\n"
          + "from sources that ARK believes to be reliable; however, ARK does not\n"
          + "guarantee the accuracy or completeness of any information obtained from any\n"
          + "third party. ARK and its clients as well as its related persons may (but do\n"
          + "not necessarily) have financial interests in securities or issuers that are\n"
          + "discussed.</p><p style=\"font-family: Arial; font-size:14px;\n"
          + "width:400px\"><strong>Investors should carefully consider the investment\n"
          + "objectives and risks as well as charges and expenses of an ARK ETF before\n"
          + "investing. This and other information are contained in the ARK ETFs&#39;\n"
          + "prospectuses, which may be obtained by visiting <a\n"
          + "href=\"https://u4959697.ct.sendgrid.net/ls/click?upn=zcd6lV4HLKVOGeJ9ek2kScEufCmUz1I3KeYPxo1NBJs-3DpNev_STQdgZzPbPnsbGBfsl6bISX3yJ6pptpG33NGbnYX5HsOAWghtIpBQA4SlDnUrr8hZIt-2BAT3SYUJlY12aWpPfyeafv-2BtEH1cvqfqbXnrWD649DNqPeAWaeO31KtZLW-2FwwpA9UzxjPi5mi9FL134BY92Dbqw8-2Fmel4MoEg9Wyz3-2BlCygsypAcaiS4-2F0wzp8MQc9MKgcL08ilZW6j0BaIeajiJruOXWg1OLR2iRjDdOszkADIaH42vxjbu-2BjFgJ2BnNMe8xLQSmtQ27qtyMuMD4EW-2BkerbRMkldjG-2FDfFI-2Fd4M-3D\" target=\"_blank\">ark-funds.com</a>. The\n"
          + "prospectus should be read carefully before investing.</strong></p><p\n"
          + "style=\"font-family: Arial; font-size:13px; width:400px\">An investment in an\n"
          + "ARK ETF is subject to risks and you can lose money on your investment in an\n"
          + "ARK ETF. There can be no assurance that the ARK ETFs will achieve their\n"
          + "investment objectives. The ARK ETFs&#39; portfolios are more volatile than\n"
          + "broad market averages. Detailed information regarding the specific risks of\n"
          + "the ARK ETFs can be found in the ARK ETFs&#39; prospectuses.</p><p\n"
          + "style=\"font-family: Arial; font-size:13px; width:400px\">Additional risks of\n"
          + "investing in ARK ETFs include market, management, concentration and\n"
          + "non-diversification risks, as well as fluctuations in market value and net\n"
          + "asset value (\"NAV\"). Shares of ETFs are bought and sold at market price\n"
          + "(not NAV) and are not individually redeemed from the ETF. ETF shares may\n"
          + "only be redeemed directly with the ETF at NAV by Authorized Participants,\n"
          + "in very large creation units. There can be no guarantee that an active\n"
          + "trading market for ETF shares will develop or be maintained, or that their\n"
          + "listing will continue or remain unchanged. Buying or selling ETF shares on\n"
          + "an exchange may require the payment of brokerage commissions and frequent\n"
          + "trading may incur brokerage costs that detract significantly from\n"
          + "investment returns.</p><p style=\"font-family: Arial; font-size:13px;\n"
          + "width:400px\">The information herein is general in nature and should not be\n"
          + "considered financial, legal or tax advice. An investor should consult a\n"
          + "financial professional, an attorney or tax professional regarding the\n"
          + "investor’s specific situation.</p><p style=\"font-family: Arial;\n"
          + "font-size:13px; width:400px\">ARK Investment Management LLC is the\n"
          + "investment adviser to the ARK ETFs.</p><p style=\"font-family: Arial;\n"
          + "font-size:13px; width:400px\">Foreside Fund Services, LLC,\n"
          + "distributor.</p><p style=\"font-family: Arial; font-size:13px;\"><a\n"
          + "href=\"https://u4959697.ct.sendgrid.net/ls/click?upn=zcd6lV4HLKVOGeJ9ek2kSaHsXj4SG4K6Hup1pZvZSI6GYZcshHITaLIKNWpgchQ6ShKlivih5Zdf4x3ZNgQfhg-3D-3Dqx06_STQdgZzPbPnsbGBfsl6bISX3yJ6pptpG33NGbnYX5HsOAWghtIpBQA4SlDnUrr8h0cb3luZ9GFa2BvB8VJJEKAnffgxvC1nL7YnNA7pNCYPdCprhDXLUnvOQJ0p7-2BJyyAOdsr7thmq2unVKRlV3yfojWtWjtkMSMbh9qd97Q3fPl-2BW5V4pFElffyQQOXhmw0B8ewb22zAaP8JNplRZkCePQEGoSJ-2F-2BPS6pOmBTYbC34EXJ9RtsofV0poWjo4zuQoSyYrRZNl4QnLO-2F3cWZf9PCsf7KfgkHvMY93H-2B3tAb5Q-3D\">Unsubscribe</a>\n"
          + "<img src=\"https://u4959697.ct.sendgrid.net/wf/open?upn=hhfldKqnf7Z825-2F0Cm2FJyCooFYcPKFe9o8W6cyMDN9TuP8-2F2hI-2BrmAyiy8xcm6aK0-2BebsAHYA7KSGKAQ1xicHymj47ivg2QkfpH2o9G7ojCH0HBDr05xBLYtJBEfJ6XoPFvJUmAcXeFO8DODGzvj0KAwM3JgjvOBJiKhm8oDacyD90SBUWZykNJairjMuFKErabOfQztVPoA6v336cVUQtC3IuILtv4DiL4AubThcNHZzye1weRp8OgZ8U44x4Eq2jgm2CLyXNCA-2FVgjNHgModVRUobNb2KZB-2FSlFLJOdOCubnYFHJIC6C4vYNXEq2b\" alt=\"\" width=\"1\" height=\"1\" border=\"0\" style=\"height:1px !important;width:1px !important;border-width:0 !important;margin-top:0 !important;margin-bottom:0 !important;margin-right:0 !important;margin-left:0 !important;padding-top:0 !important;padding-bottom:0 !important;padding-right:0 !important;padding-left:0 !important;\"/>\n"
          + "</body></html>\n";

  @BeforeEach
  void setUp() {
    MockitoAnnotations.openMocks(this);
    syncARKTradesJob = new SyncARKTradesJob(gmailService, arkEntityDao);
    when(arkEntityDao.saveArkTradeIfNotExist(any(ArkTradeEntity.class))).thenAnswer(
        (Answer<String>) invocationOnMock -> {
          var entity = (ArkTradeEntity) invocationOnMock.getArgument(0);
          if (entity != null) {
            db.putIfAbsent(entity.getTargetSymbol(), entity);
            return entity.getTargetSymbol();
          }
          return null;
        });
  }

  @AfterEach
  void tearDown() {
    db.clear();
  }

  @Test
  void testSaveArkTradeEntityIfNotExists() throws Exception {
    when(arkEntityDao.listArkTradesAvailableDate()).thenReturn(List.of("2021-03-01", "2021-03-02"));
    when(message.getContent()).thenReturn(HTML);
    when(message.getReceivedDate()).thenReturn(new Date(121, Calendar.MARCH, 22));
    when(gmailService.listEmailsBySubject(anyString(), anyString())).thenReturn(List.of(message));
    syncARKTradesJob.execute();
    Assertions.assertNotNull(db.get("U"));
    Assertions.assertEquals(11, db.size());
    Assertions.assertEquals("2021-03-22", db.get("U").getDate());
    Assertions.assertEquals("Buy", db.get("U").getOp());
    Assertions.assertEquals(187300, db.get("U").getShares());
    Assertions.assertEquals(0.005510, db.get("U").getWeight());
    Assertions.assertEquals("ARKQ", db.get("U").getSymbol());
  }

  @Test
  void testOldEmailShouldNotBeChecked() throws Exception {
    when(arkEntityDao.listArkTradesAvailableDate()).thenReturn(List.of("2021-03-01", "2021-04-02"));
    when(message.getContent()).thenReturn(HTML);
    when(message.getReceivedDate()).thenReturn(new Date(121, Calendar.MARCH, 22));
    when(gmailService.listEmailsBySubject(anyString(), anyString())).thenReturn(List.of(message));
    syncARKTradesJob.execute();
    Assertions.assertNull(db.get("U"));
    Assertions.assertEquals(0, db.size());
  }

  @Test
  void testShouldThrowExceptionIfParseFails() throws Exception {
    when(arkEntityDao.listArkTradesAvailableDate()).thenReturn(List.of("2021-03-01", "2021-03-02"));
    when(message.getContent()).thenReturn("<html>\n"
        + "<head>\n"
        + "</head>\n"
        + "<body>\n"
        + "<table>\n"
        + "  <tr>\n"
        + "    <td>Alfreds Futterkiste</td>\n"
        + "    <td>Maria Anders</td>\n"
        + "    <td>Germany</td>\n"
        + "  </tr>\n"
        + "  <tr>\n"
        + "    <td>Centro comercial Moctezuma</td>\n"
        + "    <td>Francisco Chang</td>\n"
        + "  </tr>\n"
        + "</table>\n"
        + "\n"
        + "</body>\n"
        + "</html>");
    when(message.getReceivedDate()).thenReturn(new Date(121, Calendar.MARCH, 22));
    when(gmailService.listEmailsBySubject(anyString(), anyString())).thenReturn(List.of(message));
    Assertions.assertThrows(Exception.class, () -> syncARKTradesJob.execute());
  }
}