package org.toxicaker.jobs;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.beust.jcommander.ParameterException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.toxicaker.common.Constant;
import org.toxicaker.dal.mongo.PriceEntityDao;
import org.toxicaker.dal.mongo.QuoteEntityDao;
import org.toxicaker.jobs.QuoteJob.TimeValidator;
import org.toxicaker.models.entities.PriceEntity;
import org.toxicaker.models.entities.QuoteEntity;
import org.toxicaker.models.services.Quote;
import org.toxicaker.models.services.Symbol;
import org.toxicaker.services.FmpService;
import org.toxicaker.utils.DateUtil;

class QuoteJobTest {

  @Mock
  private QuoteEntityDao quoteEntityDao;
  @Mock
  private FmpService fmpService;
  @Mock
  private PriceEntityDao priceEntityDao;

  private QuoteJob quoteJob;

  private final Map<String, QuoteEntity> quoteDB = new HashMap<>();
  private final Map<String, PriceEntity> priceDB = new HashMap<>();

  @BeforeEach
  void setUp() throws IOException {
    MockitoAnnotations.openMocks(this);
    when(fmpService.listSymbols()).thenReturn(
        List.of(Symbol.builder().symbol("MSFT").build(), Symbol.builder().symbol("ORCL").build()));
    var msftQuote = Quote.builder().symbol("MSFT").price(100.3).build();
    var orclQuote = Quote.builder().symbol("ORCL").price(21.3).build();
    when(fmpService.quotePrices("MSFT", "ORCL")).thenReturn(List.of(msftQuote, orclQuote));
    when(quoteEntityDao.saveOrUpdateQuoteEntity(any(QuoteEntity.class)))
        .thenAnswer(invocationOnMock -> {
          var entity = (QuoteEntity) invocationOnMock.getArgument(0);
          quoteDB.put(entity.getSymbol(), entity);
          return entity.getId();
        });
    when(priceEntityDao.getLatestPriceEntityUnderDate("MSFT", DateUtil.todayEST())).thenReturn(
        PriceEntity.builder().symbol("MSFT").close(90.3).build());
    when(priceEntityDao.getLatestPriceEntityUnderDate("ORCL", DateUtil.todayEST())).thenReturn(
        PriceEntity.builder().symbol("ORCL").close(25.3).build());
    when(priceEntityDao.saveOrUpdatePriceEntity(any(PriceEntity.class)))
        .thenAnswer(invocationOnMock -> {
          var entity = (PriceEntity) invocationOnMock.getArgument(0);
          priceDB.put(entity.getSymbol(), entity);
          return entity.getId();
        });
    quoteJob = new QuoteJob(fmpService, quoteEntityDao, priceEntityDao);
  }

  @AfterEach
  void tearDown() {
    quoteDB.clear();
  }

  @Test
  void testQuoteEngineShouldUpdateQuoteDB() throws Exception {
    quoteJob.run();
    Assertions.assertNotNull(quoteDB.get("MSFT"));
    Assertions.assertNotNull(quoteDB.get("ORCL"));
    Assertions.assertEquals(100.3, quoteDB.get("MSFT").getPrice());
    Assertions.assertEquals(21.3, quoteDB.get("ORCL").getPrice());
  }

  @Test
  void testQuoteEngineShouldUpdatePriceEntity() throws Exception {
    quoteJob.run();
    Assertions.assertNotNull(priceDB.get("MSFT"));
    Assertions.assertNotNull(priceDB.get("ORCL"));
    Assertions.assertEquals(10.0, priceDB.get("MSFT").getChange());
    Assertions.assertEquals(10 / 90.3, priceDB.get("MSFT").getChangePercent());
    Assertions.assertEquals(-4.0, priceDB.get("ORCL").getChange());
    Assertions.assertEquals(-4 / 25.3, priceDB.get("ORCL").getChangePercent());
  }

  @Test
  void testStartTimeAndEndTime1() throws Exception {
    quoteJob.startTime = DateUtil
        .unixToDateStr(DateUtil.timeStrToUnix(DateUtil.getCurrentTimeEST()) + 3000)
        .split(" ")[1];
    quoteJob.run();
    Assertions.assertNull(quoteDB.get("MSFT"));
    Assertions.assertNull(quoteDB.get("ORCL"));
  }

  @Test
  void testStartTimeAndEndTime2() throws Exception {
    quoteJob.endTime = DateUtil
        .unixToDateStr(DateUtil.timeStrToUnix(DateUtil.getCurrentTimeEST()) - 3000)
        .split(" ")[1];
    quoteJob.run();
    Assertions.assertNull(quoteDB.get("MSFT"));
    Assertions.assertNull(quoteDB.get("ORCL"));
  }

  @Test
  void testStartTimeAndEndTime3() throws Exception {
    quoteJob.startTime = DateUtil
        .unixToDateStr(DateUtil.timeStrToUnix(DateUtil.getCurrentTimeEST()) - 3000)
        .split(" ")[1];
    quoteJob.endTime = DateUtil
        .unixToDateStr(DateUtil.timeStrToUnix(DateUtil.getCurrentTimeEST()) + 3000)
        .split(" ")[1];
    quoteJob.run();
    Assertions.assertNotNull(quoteDB.get("MSFT"));
    Assertions.assertNotNull(quoteDB.get("ORCL"));
    Assertions.assertEquals(100.3, quoteDB.get("MSFT").getPrice());
    Assertions.assertEquals(21.3, quoteDB.get("ORCL").getPrice());
  }

  @Test
  void testTimeValidator() {
    var validator = new TimeValidator();

    Assertions.assertDoesNotThrow(() -> validator.validate("startTime", "16:00:00"));
    Assertions.assertDoesNotThrow(() -> validator.validate("startTime", "23:59:59"));
    Assertions.assertDoesNotThrow(() -> validator.validate("startTime", "01:20:40"));

    Assertions.assertThrows(ParameterException.class,
        () -> validator.validate("startTime", "3:59:59"));
    Assertions.assertThrows(ParameterException.class,
        () -> validator.validate("startTime", "03:9:59"));
    Assertions.assertThrows(ParameterException.class,
        () -> validator.validate("startTime", "26:19:59"));
    Assertions.assertThrows(ParameterException.class,
        () -> validator.validate("startTime", "23:19:100"));
    Assertions.assertThrows(ParameterException.class,
        () -> validator.validate("startTime", "20:61:59"));
  }
}