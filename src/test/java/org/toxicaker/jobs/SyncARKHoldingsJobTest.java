package org.toxicaker.jobs;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.stubbing.Answer;
import org.toxicaker.dal.mongo.ArkEntityDao;
import org.toxicaker.models.entities.ArkHoldEntity;

class SyncARKHoldingsJobTest {

  @Mock
  private ArkEntityDao arkEntityDao;

  private final Map<String, ArkHoldEntity> db = new HashMap<>();

  private SyncARKHoldingsJob syncARKHoldingsJob;

  private final static String TEST_CSV =
      "date,fund,company,ticker,cusip,shares,\"market value($)\",weight(%)\n"
          + "3/22/2021,PRNT,\"EXONE CO/THE\",XONE,302104104,1543707.00,51992051.76,9.27\n"
          + "3/22/2021,PRNT,\"HP INC\",HPQ,40434L105,985456.00,29642516.48,5.29";

  @BeforeEach
  void setUp() throws Exception {
    MockitoAnnotations.openMocks(this);
    var res = getClass().getClassLoader().getResource("temp.csv");
    assert res != null;
    FileUtils.writeStringToFile(new File(res.getPath()), TEST_CSV);
    when(arkEntityDao.saveArkHoldIfNotExist(any(ArkHoldEntity.class))).thenAnswer(
        (Answer<String>) invocationOnMock -> {
          var entity = (ArkHoldEntity) invocationOnMock.getArgument(0);
          if (entity != null) {
            db.putIfAbsent(entity.getTargetSymbol(), entity);
            return entity.getTargetSymbol();
          }
          return null;
        });
    syncARKHoldingsJob = new SyncARKHoldingsJob(arkEntityDao);
  }

  @AfterEach
  void tearDown() throws IOException {
    var res = getClass().getClassLoader().getResource("temp.csv");
    assert res != null;
    FileUtils.writeStringToFile(new File(res.getPath()), "");
    db.clear();
  }

  @Test
  void testSaveARKHoldEntityIfNotExists() throws Exception {
    try (MockedStatic<FileUtils> dummy = Mockito.mockStatic(FileUtils.class)) {
      dummy.when(() -> FileUtils.copyInputStreamToFile(any(), any()))
          .then(invocationOnMock -> null);
      syncARKHoldingsJob.execute();
      Assertions.assertNotNull(db.get("XONE"));
      Assertions.assertNotNull(db.get("HPQ"));
      Assertions.assertEquals("2021-03-22", db.get("XONE").getDate());
      Assertions.assertEquals("XONE", db.get("XONE").getTargetSymbol());
      Assertions.assertEquals(51992051.76, db.get("XONE").getMarketValue());
      Assertions.assertEquals(1543707, db.get("XONE").getShares());
    }
  }

  @Test
  void testDoNotSaveARKHoldEntityIfExists() throws Exception {

    try (MockedStatic<FileUtils> dummy = Mockito.mockStatic(FileUtils.class)) {
      dummy.when(() -> FileUtils.copyInputStreamToFile(any(), any()))
          .then(invocationOnMock -> null);
      syncARKHoldingsJob.execute();
      Assertions.assertNotNull(db.get("XONE"));
      Assertions.assertNotNull(db.get("HPQ"));
      Assertions.assertEquals("2021-03-22", db.get("XONE").getDate());
      Assertions.assertEquals("XONE", db.get("XONE").getTargetSymbol());
      Assertions.assertEquals(51992051.76, db.get("XONE").getMarketValue());
      Assertions.assertEquals(1543707, db.get("XONE").getShares());

      String testCsv =
          "date,fund,company,ticker,cusip,shares,\"market value($)\",weight(%)\n"
              + "3/22/2021,PRNT,\"EXONE CO/THE\",XONE,302104104,1.00,51992051.76,9.27\n"
              + "3/22/2021,PRNT,\"HP INC\",HPQ,40434L105,2.00,29642516.48,5.29";
      var res = getClass().getClassLoader().getResource("temp.csv");
      assert res != null;
      FileUtils.writeStringToFile(new File(res.getPath()), "");
      FileUtils.writeStringToFile(new File(res.getPath()), testCsv);

      syncARKHoldingsJob.execute();
      Assertions.assertEquals(1543707, db.get("XONE").getShares());
    }
  }

  @Test
  void testFailedToDownloadShouldThrowException() {
    try (MockedStatic<FileUtils> dummy = Mockito.mockStatic(FileUtils.class)) {
      dummy.when(() -> FileUtils.copyInputStreamToFile(any(), any()))
          .thenThrow(IOException.class);
      Assertions.assertThrows(IOException.class, () -> syncARKHoldingsJob.execute());
      Assertions.assertNull(db.get("XONE"));
      Assertions.assertNull(db.get("HPQ"));
    }
  }
}