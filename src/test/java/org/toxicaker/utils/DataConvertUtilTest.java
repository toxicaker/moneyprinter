package org.toxicaker.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.toxicaker.models.entities.ArkHoldEntity;
import org.toxicaker.models.entities.ArkTradeEntity;
import org.toxicaker.models.entities.BalanceSheetEntity;
import org.toxicaker.models.entities.CashFlowEntity;
import org.toxicaker.models.entities.CompanyProfileEntity;
import org.toxicaker.models.entities.IncomeStatementEntity;
import org.toxicaker.models.entities.NewsEntity;
import org.toxicaker.models.entities.PriceEntity;
import org.toxicaker.models.entities.QuoteEntity;
import org.toxicaker.models.services.BalanceSheet;
import org.toxicaker.models.services.CashFlow;
import org.toxicaker.models.services.CompanyProfile;
import org.toxicaker.models.services.HistoricalPrice;
import org.toxicaker.models.services.IncomeStatement;
import org.toxicaker.models.services.News;
import org.toxicaker.models.services.Quote;

class DataConvertUtilTest {

  @Test
  void toArkHold() {
    var arkHoldEntity = ArkHoldEntity.builder()
        .date("2020-01-01")
        .symbol("abc")
        .company("company")
        .targetSymbol("efg")
        .shares(1000)
        .marketValue(2000.0)
        .weight(0.2)
        .build();
    var arkHold = DataConvertUtil.toArkHold(arkHoldEntity);
    Assertions.assertNotNull(arkHold);
    Assertions.assertEquals("2020-01-01", arkHold.getDate());
    Assertions.assertEquals("abc", arkHold.getSymbol());
    Assertions.assertEquals("company", arkHold.getCompany());
    Assertions.assertEquals("efg", arkHold.getTargetSymbol());
    Assertions.assertEquals(1000, arkHold.getShares());
    Assertions.assertEquals(2000.0, arkHold.getMarketValue());
    Assertions.assertEquals(0.2, arkHold.getWeight());
    Assertions.assertEquals(0.0, arkHold.getSharesChange());
    Assertions.assertEquals(0.0, arkHold.getWeightChange());
  }

  @Test
  void toArkTrade() {
    var arkTradeEntity = ArkTradeEntity.builder()
        .date("2020-01-01")
        .symbol("abc")
        .company("company")
        .targetSymbol("efg")
        .shares(1000)
        .op("Buy")
        .weight(0.2)
        .build();
    var arkTrade = DataConvertUtil.toArkTrade(arkTradeEntity);
    Assertions.assertNotNull(arkTrade);
    Assertions.assertEquals("2020-01-01", arkTrade.getDate());
    Assertions.assertEquals("abc", arkTrade.getSymbol());
    Assertions.assertEquals("company", arkTrade.getCompany());
    Assertions.assertEquals("efg", arkTrade.getTargetSymbol());
    Assertions.assertEquals(1000, arkTrade.getShares());
    Assertions.assertEquals(0.2, arkTrade.getWeight());
    Assertions.assertEquals(0.0, arkTrade.getSharesChange());
  }

  @Test
  void toCompanyProfileEntity() {
    var companyProfile = CompanyProfile.builder().build();
    buildObject(companyProfile);
    var entity = DataConvertUtil.toCompanyProfileEntity(companyProfile);
    checkValueHelper(companyProfile, entity);
  }

  @Test
  void toCompanyProfile() {
    var entity = CompanyProfileEntity.builder().build();
    buildObject(entity);
    var profile = DataConvertUtil.toCompanyProfile(entity);
    checkValueHelper(entity, profile);
  }

  @Test
  void toNews() {
    var entity = NewsEntity.builder().build();
    buildObject(entity);
    var obj = DataConvertUtil.toNews(entity);
    checkValueHelper(entity, obj);
  }

  @Test
  void toNewsEntity() {
    var obj = News.builder().build();
    buildObject(obj);
    var entity = DataConvertUtil.toNewsEntity(obj);
    checkValueHelper(obj, entity);
  }

  @Test
  void toIncomeStatementEntity() {
    var obj = IncomeStatement.builder().build();
    buildObject(obj);
    var entity = DataConvertUtil.toIncomeStatementEntity(obj);
    checkValueHelper(obj, entity);
  }

  @Test
  void toIncomeStatement() {
    var entity = IncomeStatementEntity.builder().build();
    buildObject(entity);
    var obj = DataConvertUtil.toIncomeStatement(entity);
    checkValueHelper(entity, obj);
  }

  @Test
  void toBalanceSheetEntity() {
    var obj = BalanceSheet.builder().build();
    buildObject(obj);
    var entity = DataConvertUtil.toBalanceSheetEntity(obj);
    checkValueHelper(obj, entity);
  }

  @Test
  void toBalanceSheet() {
    var entity = BalanceSheetEntity.builder().build();
    buildObject(entity);
    var obj = DataConvertUtil.toBalanceSheet(entity);
    checkValueHelper(entity, obj);
  }

  @Test
  void toCashFlowEntity() {
    var obj = CashFlow.builder().build();
    buildObject(obj);
    var entity = DataConvertUtil.toCashFlowEntity(obj);
    checkValueHelper(obj, entity);
  }

  @Test
  void toCashFlow() {
    var entity = CashFlowEntity.builder().build();
    buildObject(entity);
    var obj = DataConvertUtil.toCashFlow(entity);
    checkValueHelper(entity, obj);
  }

  @Test
  void toPriceEntity() {
    var obj = HistoricalPrice.builder().build();
    buildObject(obj);
    var entity = DataConvertUtil.toPriceEntity(obj);
    checkValueHelper(obj, entity);
  }

  @Test
  void toHistoricalPrice() {
    var entity = PriceEntity.builder().build();
    buildObject(entity);
    var obj = DataConvertUtil.toHistoricalPrice(entity);
    checkValueHelper(entity, obj);
  }

  @Test
  void toQuote() {
    var entity = QuoteEntity.builder().build();
    buildObject(entity);
    var obj = DataConvertUtil.toQuote(entity);
    checkValueHelper(entity, obj);
  }

  @Test
  void toQuoteEntity() {
    var obj = Quote.builder().build();
    buildObject(obj);
    var entity = DataConvertUtil.toQuoteEntity(obj);
    checkValueHelper(obj, entity);
  }

  void buildObject(Object object) {
    var clazz = object.getClass();
    var fields = clazz.getDeclaredFields();
    for (var f : fields) {
      try {
        f.setAccessible(true);
        if (String.class.equals(f.getType())) {
          f.set(object, "test");
        }
        if (Double.class.equals(f.getType()) || double.class.equals(f.getType())) {
          f.set(object, 123.1);
        }

        if (Long.class.equals(f.getType()) || long.class.equals(f.getType())) {
          f.set(object, 121321L);
        }
        if (Integer.class.equals(f.getType()) || int.class.equals(f.getType())) {
          f.set(object, 78432);
        }
        if (Boolean.class.equals(f.getType()) || boolean.class.equals(f.getType())) {
          f.set(object, true);
        }
      } catch (IllegalAccessException e) {
        e.printStackTrace();
      }
    }
  }

  void checkValueHelper(Object input, Object output) {
    var inputClass = input.getClass();
    var outputClass = output.getClass();
    var fields = inputClass.getDeclaredFields();
    for (var f : fields) {
      try {
        f.setAccessible(true);
        var name = f.getName();
        var val1 = f.get(input);
        var targetField = outputClass.getDeclaredField(name);
        targetField.setAccessible(true);
        var val2 = targetField.get(output);
        Assertions.assertEquals(val1, val2);
      } catch (NoSuchFieldException | IllegalAccessException ex) {
        // ignore
      }
    }
  }
}