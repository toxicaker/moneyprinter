package org.toxicaker.common;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {


  @ExceptionHandler(value = Exception.class)
  @ResponseBody
  public JsonResp handleException(Exception e) {
    doLog(e);
    if (e instanceof MissingServletRequestParameterException
        || e instanceof MethodArgumentTypeMismatchException
        || e instanceof HttpRequestMethodNotSupportedException) {
      return new JsonResp(JsonResp.FAILURE, "parameter error: " + e.getMessage(), null);
    }
    return new JsonResp(JsonResp.FAILURE, e.getMessage(), null);
  }

  private void doLog(Exception ex) {
    if (ex instanceof ServiceException) {
      ServiceException e = (ServiceException) ex;
      log.error("service exception: " + e.getMessage(), ex);
    } else if (ex instanceof MissingServletRequestParameterException ||
        ex instanceof MethodArgumentTypeMismatchException ||
        ex instanceof HttpRequestMethodNotSupportedException) {
      log.warn("parameter error", ex);
    } else if (ex != null) {
      log.error("unknown error", ex);
    }
  }
}