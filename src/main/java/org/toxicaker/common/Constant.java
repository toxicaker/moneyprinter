package org.toxicaker.common;

import java.io.IOException;
import java.util.Properties;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Constant {

  /**
   * Gmail settings
   */
  public static String EMAIL_USERNAME;

  public static String EMAIL_PASSWORD;

  public static String EMAIL_HOST;

  /**
   * API Keys
   */

  public static String FMP_KEY;

  public static String FINNHUB_KEY;

  public static String ENV;

  public static String MONGO_DB_NAME;

  public static String MONGO_DB_HOST;

  public static String MONGO_DB_USERNAME;

  public static String MONGO_DB_PASSWORD;

  public static int MONGO_DB_PORT;

  public static int MONGO_DB_CONNECTION_TIMEOUT;


  public static void load(String env) {
    try {
      var prop = new Properties();
      switch (env) {
        case "prod":
          prop.load(Constant.class.getClassLoader().getResourceAsStream("application.properties"));
          log.info("Loaded configuration application.properties");
          break;
        case "test":
          prop.load(
              Constant.class.getClassLoader().getResourceAsStream("application-test.properties"));
          log.info("Loaded configuration application-test.properties");
          break;
        default:
          prop.load(
              Constant.class.getClassLoader().getResourceAsStream("application-dev.properties"));
          log.info("Loaded configuration application-dev.properties");
          break;
      }
      EMAIL_USERNAME = prop.getProperty("email.username");
      EMAIL_PASSWORD = prop.getProperty("email.password");
      EMAIL_HOST = prop.getProperty("email.host");
      FMP_KEY = prop.getProperty("fmp.key");
      FINNHUB_KEY = prop.getProperty("finnhub.key");
      ENV = prop.getProperty("env");
      MONGO_DB_NAME = prop.getProperty("mongo.db.name");
      MONGO_DB_HOST = prop.getProperty("mongo.db.host");
      MONGO_DB_PORT = Integer.parseInt(prop.getProperty("mongo.db.port"));
      MONGO_DB_USERNAME = prop.getProperty("mongo.db.username");
      MONGO_DB_PASSWORD = prop.getProperty("mongo.db.password");
      MONGO_DB_CONNECTION_TIMEOUT = Integer.parseInt(prop.getProperty("mongo.connection.timeout"));
    } catch (IOException e) {
      log.error("Failed to load application.properties in resources folder.", e);
    }
  }
}
