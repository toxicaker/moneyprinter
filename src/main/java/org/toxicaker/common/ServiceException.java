package org.toxicaker.common;

public class ServiceException extends Exception {
  public ServiceException(String message) {
    super(message);
  }
}
