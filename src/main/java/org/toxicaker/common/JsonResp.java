package org.toxicaker.common;

import lombok.Data;

@Data
public class JsonResp {

  public static final int SUCCESS = 1;
  public static final int FAILURE = -1;

  private int code = SUCCESS;
  private String message = "success";
  private Object data;

  public JsonResp(int code, String message, Object data) {
    this.code = code;
    this.message = message;
    this.data = data;
  }

  public JsonResp(Object data) {
    this.data = data;
  }
}
