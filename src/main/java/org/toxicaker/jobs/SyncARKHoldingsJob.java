package org.toxicaker.jobs;

import com.beust.jcommander.Parameters;
import com.opencsv.CSVReader;
import java.io.File;
import java.io.FileReader;
import java.net.URL;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.toxicaker.dal.mongo.ArkEntityDao;
import org.toxicaker.models.entities.ArkHoldEntity;

/**
 * The background job for ARK data synchronization. Sync up ARK stock holdings.
 */
@Slf4j
@Parameters(commandDescription = "The background job for ARK data synchronization. Sync up ARK stock holdings.")
public class SyncARKHoldingsJob extends Job {

  private final ArkEntityDao arkEntityDao;

  public SyncARKHoldingsJob(ArkEntityDao arkEntityDao) {
    this.arkEntityDao = arkEntityDao;
  }

  @Override
  protected void run() throws Exception {
    var urls = java.util.List.of(
        "https://ark-funds.com/wp-content/fundsiteliterature/csv/ARK_INNOVATION_ETF_ARKK_HOLDINGS.csv",
        "https://ark-funds.com/wp-content/fundsiteliterature/csv/ARK_AUTONOMOUS_TECHNOLOGY_&_ROBOTICS_ETF_ARKQ_HOLDINGS.csv",
        "https://ark-funds.com/wp-content/fundsiteliterature/csv/ARK_NEXT_GENERATION_INTERNET_ETF_ARKW_HOLDINGS.csv",
        "https://ark-funds.com/wp-content/fundsiteliterature/csv/ARK_GENOMIC_REVOLUTION_MULTISECTOR_ETF_ARKG_HOLDINGS.csv",
        "https://ark-funds.com/wp-content/fundsiteliterature/csv/ARK_FINTECH_INNOVATION_ETF_ARKF_HOLDINGS.csv",
        "https://ark-funds.com/wp-content/fundsiteliterature/csv/THE_3D_PRINTING_ETF_PRNT_HOLDINGS.csv",
        "https://ark-funds.com/wp-content/fundsiteliterature/csv/ARK_ISRAEL_INNOVATIVE_TECHNOLOGY_ETF_IZRL_HOLDINGS.csv"
    );

    var res = getClass().getClassLoader().getResource("temp.csv");
    assert res != null;
    for (String url : urls) {
      log.info("Started downloading data from {}", url);
      var urlObject = new URL(url);
      var conn = urlObject.openConnection();
      conn.setRequestProperty("User-Agent",
          "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:31.0) Gecko/20100101 Firefox/31.0");
      FileUtils.copyInputStreamToFile(conn.getInputStream(), new File(res.getPath()));
      CSVReader reader = new CSVReader(new FileReader(res.getPath()));
      List<String[]> rows = reader.readAll();
      for (int i = 1; i < rows.size(); i++) {
        var r = rows.get(i);
        var strs = r[0].split("/");
        if (strs.length < 3) {
          continue;
        }
        var month = strs[0];
        if (month.length() == 1) {
          month = "0" + month;
        }
        var day = strs[1];
        if (day.length() == 1) {
          day = "0" + day;
        }
        var date = strs[2] + "-" + month + "-" + day;
        var arkHoldEntity = ArkHoldEntity
            .builder()
            .date(date)
            .symbol(r[1])
            .company(r[2])
            .targetSymbol(r[3])
            .shares((int) Double.parseDouble(r[5]))
            .marketValue(Double.parseDouble(r[6]))
            .weight(Double.parseDouble(r[7]) / 100)
            .build();
        arkEntityDao.saveArkHoldIfNotExist(arkHoldEntity);
      }
    }
  }

  @Override
  public String jobName() {
    return "SYNC ARK HOLDINGS";
  }
}
