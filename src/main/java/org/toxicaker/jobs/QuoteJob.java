package org.toxicaker.jobs;

import com.beust.jcommander.IValueValidator;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.toxicaker.dal.mongo.PriceEntityDao;
import org.toxicaker.dal.mongo.QuoteEntityDao;
import org.toxicaker.models.entities.PriceEntity;
import org.toxicaker.models.services.Symbol;
import org.toxicaker.services.FmpService;
import org.toxicaker.utils.DataConvertUtil;
import org.toxicaker.utils.DateUtil;

/**
 * The background job for quoting stock real-time metrics 1. Update quote metrics 2. Update OHLC
 * <pre>
 * https://fmpcloud.io/documentation#realtimeQuote
 * </pre>
 */
@Slf4j
@Parameters(commandDescription = "The background job for quoting stock real-time metrics and saving into DB.")
public class QuoteJob extends Job {

  private final QuoteEntityDao quoteEntityDao;
  private final FmpService fmpService;
  private final PriceEntityDao priceEntityDao;

  private final ExecutorService executorService = Executors.newFixedThreadPool(8);

  @Parameter(names = "--startTime", description = "When does the quote engine start(EST Time). Format: 09:30:00", validateValueWith = TimeValidator.class)
  protected String startTime;
  @Parameter(names = "--endTime", description = "When does the quote engine end(EST Time). Format: 16:00:00", validateValueWith = TimeValidator.class)
  protected String endTime;

  public QuoteJob(FmpService fmpService, QuoteEntityDao quoteEntityDao,
      PriceEntityDao priceEntityDao) {
    this.quoteEntityDao = quoteEntityDao;
    this.fmpService = fmpService;
    this.priceEntityDao = priceEntityDao;
  }

  @Override
  protected void run() throws Exception {

    var symbols = fmpService.listSymbols();
    log.info("Total symbol number: {}", symbols.size());
    Map<String, PriceEntity> lastPriceEntities = new HashMap<>();
    var filter = new HashSet<>();
    while (true) {
      var currentTimeEST = DateUtil.getCurrentTimeEST();
      var time = currentTimeEST.split(" ")[1];

      if (StringUtils.isNotEmpty(startTime)) {
        if (time.compareTo(startTime) < 0) {
          log.info("Start time: {}. Current time {}", startTime, time);
          break;
        }
      }
      if (StringUtils.isNotEmpty(endTime)) {
        if (time.compareTo(endTime) > 0) {
          log.info("End time: {}. Current time {}", startTime, time);
          break;
        }
      }
      int taskNum = (int) Math.ceil(symbols.size() / 1000.0);
      var latch = new CountDownLatch(taskNum);
      for (int i = 0; i < symbols.size(); i += 1000) {
        int start = i;
        int end = Math.min(i + 1000, symbols.size());
        Runnable task = () -> {
          var sub = symbols
              .subList(start, end)
              .stream()
              .map(Symbol::getSymbol)
              .toArray(String[]::new);
          try {
            var quotes = fmpService.quotePrices(sub);
            for (var q : quotes) {
              if (filter.contains(q.getSymbol())) {
                continue;
              }
              try {
                quoteEntityDao.saveOrUpdateQuoteEntity(DataConvertUtil.toQuoteEntity(q));
                var lastPriceEntity = lastPriceEntities.get(q.getSymbol());
                if (lastPriceEntity == null) {
                  lastPriceEntity = priceEntityDao
                      .getLatestPriceEntityUnderDate(q.getSymbol(), DateUtil.todayEST());
                  lastPriceEntities.put(q.getSymbol(), lastPriceEntity);
                }
                var change = 0.0;
                var changePercent = 0.0;
                if (lastPriceEntity != null) {
                  change = q.getPrice() - lastPriceEntity.getClose();
                  changePercent = lastPriceEntity.getClose() == 0.0 ?
                      0.0 : change / lastPriceEntity.getClose();
                }
                var priceEntity = PriceEntity.builder()
                    .open(q.getOpen())
                    .close(q.getPrice())
                    .low(q.getDayLow())
                    .high(q.getDayHigh())
                    .volume(q.getVolume())
                    .symbol(q.getSymbol())
                    .date(DateUtil.todayEST())
                    .change(change)
                    .changePercent(changePercent)
                    .build();
                priceEntityDao.saveOrUpdatePriceEntity(priceEntity);
              } catch (Exception ex) {
                log.error("Failed to quote {}", q, ex);
                filter.add(q.getSymbol());
              }
            }
          } catch (Exception ex) {
            log.error("Failed to quote: {}", sub, ex);
          } finally {
            latch.countDown();
          }
        };
        executorService.submit(task);
      }
      latch.await();
      log.info("Round finished: {}s", count());
      reset();
      log.info("Failed quotes: {}", filter);
      if (StringUtils.isEmpty(endTime)) {
        log.info("EndTime is empty");
        break;
      }
    }
    executorService.shutdown();
  }

  @Override
  public String jobName() {
    return "QUOTE ENGINE";
  }


  public static class TimeValidator implements IValueValidator<String> {

    @Override
    public void validate(String key, String value) throws ParameterException {
      boolean invalid = false;
      if (StringUtils.isEmpty(value)) {
        invalid = true;
      }
      if (value.length() != "16:00:00".length()) {
        invalid = true;
      }
      try {
        var h = Integer.parseInt(value.split(":")[0]);
        var m = Integer.parseInt(value.split(":")[1]);
        var s = Integer.parseInt(value.split(":")[2]);
        if (h < 0 || h > 23) {
          invalid = true;
        }
        if (m < 0 || m > 59) {
          invalid = true;
        }
        if (s < 0 || s > 59) {
          invalid = true;
        }
      } catch (Exception ex) {
        throw new ParameterException(
            "Parameter " + key + " should be time format. Ex: 16:00:00 (found " + value + ")");
      }
      if (invalid) {
        throw new ParameterException(
            "Parameter " + key + " should be time format. Ex: 16:00:00 (found " + value + ")");
      }
    }
  }
}
