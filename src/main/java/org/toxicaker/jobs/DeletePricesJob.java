package org.toxicaker.jobs;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import java.util.Comparator;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.toxicaker.dal.mongo.PriceEntityDao;
import org.toxicaker.models.services.HistoricalPrice;
import org.toxicaker.services.FmpService;
import org.toxicaker.utils.DataConvertUtil;
import org.toxicaker.utils.DateUtil;

@Slf4j
@Parameters(commandDescription = "Delete price for the specific date or a time range")
public class DeletePricesJob extends Job {

  private final PriceEntityDao priceEntityDao;

  @Parameter(names = "--startDate", description = "The start date. Format: 2021-01-01")
  protected String startDate;
  @Parameter(names = "--endDate", description = "The end date. If not specified, only delete the price of startDate. Format: 2021-01-01")
  protected String endDate;

  public DeletePricesJob(PriceEntityDao priceEntityDao) {
    this.priceEntityDao = priceEntityDao;
  }

  @Override
  protected void run() throws Exception {
    var date = startDate;
    do {
      priceEntityDao.deletePriceEntitiesDate(date);
      date = DateUtil.tomorrow(date);
    } while (StringUtils.isNotEmpty(endDate) && !date.equals(DateUtil.tomorrow(endDate)));
  }

  @Override
  public String jobName() {
    return "DELETE PRICES";
  }
}
