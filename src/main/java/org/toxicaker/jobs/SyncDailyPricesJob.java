package org.toxicaker.jobs;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.toxicaker.dal.mongo.PriceEntityDao;
import org.toxicaker.models.services.HistoricalPrice;
import org.toxicaker.models.services.Symbol;
import org.toxicaker.services.FmpService;
import org.toxicaker.utils.DataConvertUtil;
import org.toxicaker.utils.DateUtil;

/**
 * The background job for daily prices sync up
 */
@Slf4j
@Parameters(commandDescription = "The background job for daily prices data synchronization.")
public class SyncDailyPricesJob extends Job {

  private final PriceEntityDao priceEntityDao;
  private final FmpService fmpService;

  @Parameter(names = "--startDate", description = "The initial start date of daily stock prices. Format: 2021-01-01")
  protected String startDate;

  public SyncDailyPricesJob(PriceEntityDao priceEntityDao,
      FmpService fmpService) {

    this.priceEntityDao = priceEntityDao;
    this.fmpService = fmpService;
  }

  @Override
  protected void run() throws Exception {
    var symbols = fmpService.listSymbols().stream().map(Symbol::getSymbol).collect(
        Collectors.toList());
    log.info("Total symbol number: {}", symbols.size());
    var emptySet = new ArrayList<String>();
    syncSymbolPrices(symbols, emptySet);
    log.info("{} symbols have empty prices: {}. Retrying", emptySet.size(), emptySet);
    syncSymbolPrices(emptySet, new ArrayList<>());
  }

  private void syncSymbolPrices(List<String> symbols, List<String> emptySet) {
    for (int i = 0; i < symbols.size(); i++) {
      try {
        var symbol = symbols.get(i);
        var prices = queryPricesWithRetry(symbol, DateUtil.yesterday(startDate), DateUtil.today(),
            5);
        prices.sort(Comparator.comparing(HistoricalPrice::getDate));
        if (prices.isEmpty()) {
          emptySet.add(symbol);
        }
        for (int j = 1; j < prices.size(); j++) {
          var cur = prices.get(j);
          var prev = prices.get(j - 1);
          var change = cur.getClose() - prev.getClose();
          var changePercent = prev.getClose() == 0.0 ? 0.0 : change / prev.getClose();
          cur.setChange(change);
          cur.setChangePercent(changePercent);
          priceEntityDao.saveOrUpdatePriceEntity(DataConvertUtil.toPriceEntity(cur));
        }
        log.info("Progress: {}/{}. Time: {}s", i, symbols.size(), count());
      } catch (Exception ex) {
        log.error("Failed to sync up the prices of {}", symbols.get(i), ex);
      }
    }
  }

  private List<HistoricalPrice> queryPricesWithRetry(String symbol, String startDate,
      String endDate, int retry) throws Exception {
    List<HistoricalPrice> rst = new ArrayList<>();
    if (retry == 0) {
      return rst;
    }
    rst = fmpService.listDailyPrice(symbol, startDate, endDate);
    if (rst.isEmpty()) {
      retry--;
      Thread.sleep(200);
      return queryPricesWithRetry(symbol, startDate, endDate, retry);
    }
    return rst;
  }

  @Override
  public String jobName() {
    return "SYNC DAILY PRICES";
  }
}
