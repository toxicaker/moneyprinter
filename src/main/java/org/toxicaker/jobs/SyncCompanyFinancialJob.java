package org.toxicaker.jobs;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import lombok.extern.slf4j.Slf4j;
import org.toxicaker.dal.mongo.CompanyDao;
import org.toxicaker.services.FmpService;
import org.toxicaker.utils.DataConvertUtil;

/**
 * Sync up company financial reports. If the data doesn't exist, it will save it into DB. Otherwise
 * it will check and update.
 */
@Slf4j
@Parameters(commandDescription =
    "Sync up company financial reports. If the data doesn't exist, it will save it into DB. "
        + "Otherwise it will check and update.")
public class SyncCompanyFinancialJob extends Job {

  @Parameter(names = {"--limit",
      "-l"}, description = "The number of financial reports that get checked and updated")
  private int limit = 1;

  @Parameter(names = "ic", description = "Update income statements if specified")
  private boolean ic;

  @Parameter(names = "bs", description = "Update balance sheets if specified")
  private boolean bs;

  @Parameter(names = "cf", description = "Update cash flow statements if specified")
  private boolean cf;

  private final CompanyDao companyDao;
  private final FmpService fmpService;

  public SyncCompanyFinancialJob(CompanyDao companyDao, FmpService fmpService) {
    this.companyDao = companyDao;
    this.fmpService = fmpService;
  }

  @Override
  protected void run() throws Exception {
    var symbolsWithFinancial = fmpService.listSymbolsWithFinancialReports();
    log.info("Total symbol number with financial reports: {}", symbolsWithFinancial.size());
    for (int i = 0; i < symbolsWithFinancial.size(); i++) {
      var symbol = symbolsWithFinancial.get(i);
      try {
        // income statement
        if (ic) {
          var yearlyIncomeStatement = fmpService.listIncomeStatements(symbol, false, limit);
          Thread.sleep(100);
          var quarterlyIncomeStatement = fmpService.listIncomeStatements(symbol, true, limit);
          Thread.sleep(100);
          if (yearlyIncomeStatement.size() > 0) {
            var entity = DataConvertUtil
                .toIncomeStatementEntity(yearlyIncomeStatement.get(limit - 1));
            companyDao.saveIncomeStatementEntityIfNotExists(entity);
          }
          if (quarterlyIncomeStatement.size() > 0) {
            var entity = DataConvertUtil
                .toIncomeStatementEntity(quarterlyIncomeStatement.get(limit - 1));
            companyDao.saveIncomeStatementEntityIfNotExists(entity);
          }
        }
        if (bs) {
          // balance sheet
          var yearlyBalanceSheet = fmpService.listBalanceSheets(symbol, false, limit);
          Thread.sleep(100);
          var quarterlyBalanceSheet = fmpService.listBalanceSheets(symbol, true, limit);
          Thread.sleep(100);
          if (yearlyBalanceSheet.size() > 0) {
            var entity = DataConvertUtil.toBalanceSheetEntity(yearlyBalanceSheet.get(limit - 1));
            companyDao.saveBalanceSheetEntityIfNotExists(entity);
          }
          if (quarterlyBalanceSheet.size() > 0) {
            var entity = DataConvertUtil.toBalanceSheetEntity(quarterlyBalanceSheet.get(limit - 1));
            companyDao.saveBalanceSheetEntityIfNotExists(entity);
          }
        }
        if (cf) {
          // cash flow
          var yearlyCashFlow = fmpService.listCashFlows(symbol, false, limit);
          Thread.sleep(100);
          var quarterlyCashFlow = fmpService.listCashFlows(symbol, true, limit);
          Thread.sleep(100);
          if (yearlyCashFlow.size() > 0) {
            var entity = DataConvertUtil.toCashFlowEntity(yearlyCashFlow.get(limit - 1));
            companyDao.saveCashFlowEntityIfNotExists(entity);
          }
          if (quarterlyCashFlow.size() > 0) {
            var entity = DataConvertUtil.toCashFlowEntity(quarterlyCashFlow.get(limit - 1));
            companyDao.saveCashFlowEntityIfNotExists(entity);
          }
        }
      } catch (Exception ex) {
        log.error("Failed to sync financial statements for {}", symbol, ex);
      }
      if (i % 10 == 0) {
        log.info("Progress: {}/{}: Time: {}s", i, symbolsWithFinancial.size(), count());
      }
    }
  }

  @Override
  public String jobName() {
    return "SYNC FINANCIAL REPORTS";
  }
}
