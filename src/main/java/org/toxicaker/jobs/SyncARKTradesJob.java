package org.toxicaker.jobs;

import com.beust.jcommander.Parameters;
import javax.mail.Message;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.select.Elements;
import org.toxicaker.dal.mongo.ArkEntityDao;
import org.toxicaker.models.entities.ArkTradeEntity;
import org.toxicaker.services.GmailService;
import org.toxicaker.utils.DateUtil;

/**
 * The background job for ARK data synchronization. Sync up ARK trades information.
 */
@Slf4j
@Parameters(commandDescription = "The background job for ARK data synchronization. Sync up ARK trades information.")
public class SyncARKTradesJob extends Job {

  private final GmailService gmailService;
  private final ArkEntityDao arkEntityDao;

  public SyncARKTradesJob(GmailService gmailService,
      ArkEntityDao arkEntityDao) {
    this.gmailService = gmailService;
    this.arkEntityDao = arkEntityDao;
  }

  private static final String SUBJECT1 = "Daily Trade Information";
  private static final String SUBJECT2 = "ARK Investment Management Trading Information";

  @Override
  protected void run() throws Exception {
    var dates = arkEntityDao.listArkTradesAvailableDate();
    var latest = dates.size() > 0 ? dates.get(dates.size() - 1) : "1960-01-01";
    var messages = gmailService.listEmailsBySubject(SUBJECT1, SUBJECT2);
    for (Message m : messages) {
      var date = DateUtil.dateToDateStr(m.getReceivedDate());
      if (date.compareTo(latest) >= 0) {
        var body = m.getContent().toString();
        log.info("Message date: {}. Body: {}", date, body);
        var doc = Jsoup.parse(body);
        var row = doc.select("tr");
        for (int i = 1; i < row.size(); i++) {
          var r = row.get(i);
          Elements elements = r.select("td");

          var strs = elements.get(2).text().split("/");
          if (strs.length < 3) {
            continue;
          }
          var month = strs[0];
          if (month.length() == 1) {
            month = "0" + month;
          }
          var day = strs[1];
          if (day.length() == 1) {
            day = "0" + day;
          }
          var arkTradeEntity = ArkTradeEntity.builder()
              .symbol(elements.get(1).text())
              .date(strs[2] + "-" + month + "-" + day)
              .op(elements.get(3).text())
              .targetSymbol(elements.get(4).text())
              .company(elements.get(6).text())
              .shares(Integer.parseInt(elements.get(7).text().replace(",", "")))
              .weight(Double.parseDouble(elements.get(8).text()) / 100)
              .build();
          arkEntityDao.saveArkTradeIfNotExist(arkTradeEntity);
        }
      }
    }
  }

  @Override
  public String jobName() {
    return "SYNC ARK TRADES";
  }
}
