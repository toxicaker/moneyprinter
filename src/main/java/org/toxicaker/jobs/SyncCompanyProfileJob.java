package org.toxicaker.jobs;

import com.beust.jcommander.Parameters;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.toxicaker.dal.mongo.CompanyDao;
import org.toxicaker.models.services.Symbol;
import org.toxicaker.services.FmpService;
import org.toxicaker.utils.DataConvertUtil;

/**
 * Sync up company profile data. If the data doesn't exist, it will save it into DB. Otherwise it
 * will check and update.
 */
@Slf4j
@Parameters(commandDescription =
    "Sync up company profile data. If the data doesn't exist, it will save it into DB. Otherwise it will check and update.")
public class SyncCompanyProfileJob extends Job {

  private final CompanyDao companyDao;
  private final FmpService fmpService;

  public SyncCompanyProfileJob(CompanyDao companyDao, FmpService fmpService) {
    this.companyDao = companyDao;
    this.fmpService = fmpService;
  }

  @Override
  protected void run() throws Exception {
    var symbols = fmpService.listSymbols();
    log.info("Total symbol number: {}", symbols.size());
    for (int i = 0; i < symbols.size(); i += 100) {
      int j = Math.min(i + 100, symbols.size());
      var sub = symbols.subList(i, j).stream().map(Symbol::getSymbol)
          .collect(Collectors.toList());
      try {
        var companyProfiles = fmpService.listCompanyProfiles(sub);
        for (var companyProfile : companyProfiles) {
          companyDao.saveOrUpdateCompanyProfileEntity(
              DataConvertUtil.toCompanyProfileEntity(companyProfile));
        }
        log.info("Progress: {}/{}. Time: {}", j, symbols.size(), count());
      } catch (Exception ex) {
        log.error("Failed to sync company profiles: {}", sub, ex);
      }
    }
  }

  @Override
  public String jobName() {
    return "SYNC UP COMPANY PROFILE";
  }
}
