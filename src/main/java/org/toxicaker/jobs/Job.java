package org.toxicaker.jobs;

import com.beust.jcommander.Parameter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class Job {

  @Parameter(names = {"--help", "-h"}, help = true, description = "Menu")
  public boolean help = false;

  @Parameter(names = {"--env", "-e"},
      description = "The environment. Support values: prod|test|dev", required = true)
  public String env;

  private static long startTime;

  protected static void start() {
    startTime = System.currentTimeMillis();
  }

  protected static void reset() {
    startTime = 0;
  }

  protected static double count() {
    var elapsedTime = System.currentTimeMillis() - startTime;
    return (double) elapsedTime / 1000;
  }

  protected abstract void run() throws Exception;

  public abstract String jobName();

  public void execute() throws Exception {
    try {
      log.info(">>>>>>>>>>>>>>>>>>>>> Started job {} <<<<<<<<<<<<<<<<<<<<<<<<", jobName());
      start();
      run();
    } catch (Exception ex) {
      log.error("Failed to execute job {}", jobName(), ex);
      throw ex;
    } finally {
      log.info(">>>>>>>>>>>>>>>>>>>>> Job {} finished: {}s <<<<<<<<<<<<<<<<<<<<<<<<", jobName(), count());
      reset();
    }
  }
}
