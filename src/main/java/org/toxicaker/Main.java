package org.toxicaker;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration;
import org.springframework.boot.autoconfigure.data.mongo.MongoRepositoriesAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.toxicaker.common.Constant;
import org.toxicaker.dal.mongo.ArkEntityDao;
import org.toxicaker.dal.mongo.CompanyDao;
import org.toxicaker.dal.mongo.PriceEntityDao;
import org.toxicaker.dal.mongo.QuoteEntityDao;
import org.toxicaker.jobs.DeletePricesJob;
import org.toxicaker.jobs.QuoteJob;
import org.toxicaker.jobs.SyncARKHoldingsJob;
import org.toxicaker.jobs.SyncARKTradesJob;
import org.toxicaker.jobs.SyncCompanyFinancialJob;
import org.toxicaker.jobs.SyncCompanyProfileJob;
import org.toxicaker.jobs.SyncDailyPricesJob;
import org.toxicaker.services.impl.FmpServiceImpl;
import org.toxicaker.services.impl.GmailServiceImpl;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication(exclude = {MongoAutoConfiguration.class,
    MongoRepositoriesAutoConfiguration.class,
    MongoDataAutoConfiguration.class})
@EnableSwagger2
@Slf4j
@Parameters(commandDescription = "API server.")
public class Main {

  @Parameter(names = {"--env", "-e"},
      description = "The environment. Support values: prod|test|dev", required = true)
  private String env;
  @Parameter(names = {"--help", "-h"}, help = true, description = "Menu")
  private boolean help = false;

  public static String PROD = "prod";
  public static String TEST = "test";
  public static String DEV = "dev";

  /**
   * Command List
   */
  private static final String COMMAND_API_SERVER = "api-server";
  private static final String COMMAND_COMPANY_FINANCIAL = "company-financial";
  private static final String COMMAND_COMPANY_PROFILE = "company-profile";
  private static final String COMMAND_DAILY_PRICES = "daily-prices";
  private static final String COMMAND_ARK_HOLDINGS = "ark-holdings";
  private static final String COMMAND_ARK_TRADES = "ark-trades";
  private static final String COMMAND_QUOTE_ENGINE = "quote-engine";
  private static final String COMMAND_DELETE_PRICES = "delete-price";


  public static void main(String[] args) throws Exception {
    var companyDao = new CompanyDao();
    var gmailService = new GmailServiceImpl();
    var fmpService = new FmpServiceImpl();
    var arkEntityDao = new ArkEntityDao();
    var quoteEntityDao = new QuoteEntityDao();
    var priceEntityDao = new PriceEntityDao();

    var main = new Main();
    var syncCompanyProfileJob = new SyncCompanyProfileJob(companyDao, fmpService);
    var syncCompanyFinancialJob = new SyncCompanyFinancialJob(companyDao, fmpService);
    var syncARKHoldingsJob = new SyncARKHoldingsJob(arkEntityDao);
    var syncARKTradesJob = new SyncARKTradesJob(gmailService, arkEntityDao);
    var quoteEngine = new QuoteJob(fmpService, quoteEntityDao, priceEntityDao);
    var dailyPriceJob = new SyncDailyPricesJob(priceEntityDao, fmpService);
    var deletePricesJob = new DeletePricesJob(priceEntityDao);

    JCommander jCommander = new JCommander();
    jCommander.addCommand(COMMAND_API_SERVER, main);
    jCommander.addCommand(COMMAND_COMPANY_PROFILE, syncCompanyProfileJob);
    jCommander.addCommand(COMMAND_COMPANY_FINANCIAL, syncCompanyFinancialJob);
    jCommander.addCommand(COMMAND_ARK_HOLDINGS, syncARKHoldingsJob);
    jCommander.addCommand(COMMAND_ARK_TRADES, syncARKTradesJob);
    jCommander.addCommand(COMMAND_QUOTE_ENGINE, quoteEngine);
    jCommander.addCommand(COMMAND_DAILY_PRICES, dailyPriceJob);
    jCommander.addCommand(COMMAND_DELETE_PRICES, deletePricesJob);
    jCommander.parse(args);

    if (jCommander.getParsedCommand() == null) {
      printCommands(jCommander);
      return;
    }

    switch (jCommander.getParsedCommand()) {
      case COMMAND_API_SERVER:
        if (main.help) {
          jCommander.usage(COMMAND_API_SERVER);
          return;
        }
        Constant.load(main.env);
        SpringApplication.run(Main.class, args);
        break;
      case COMMAND_COMPANY_FINANCIAL:
        if (syncCompanyFinancialJob.help) {
          jCommander.usage(COMMAND_COMPANY_FINANCIAL);
          return;
        }
        Constant.load(syncCompanyFinancialJob.env);
        syncCompanyFinancialJob.execute();
        break;
      case COMMAND_COMPANY_PROFILE:
        if (syncCompanyProfileJob.help) {
          jCommander.usage(COMMAND_COMPANY_PROFILE);
          return;
        }
        Constant.load(syncCompanyProfileJob.env);
        syncCompanyProfileJob.execute();
        break;
      case COMMAND_ARK_HOLDINGS:
        if (syncARKHoldingsJob.help) {
          jCommander.usage(COMMAND_ARK_HOLDINGS);
          return;
        }
        Constant.load(syncARKHoldingsJob.env);
        syncARKHoldingsJob.execute();
        break;
      case COMMAND_ARK_TRADES:
        if (syncARKTradesJob.help) {
          jCommander.usage(COMMAND_ARK_TRADES);
          return;
        }
        Constant.load(syncARKTradesJob.env);
        syncARKTradesJob.execute();
        break;
      case COMMAND_QUOTE_ENGINE:
        if (quoteEngine.help) {
          jCommander.usage(COMMAND_QUOTE_ENGINE);
          return;
        }
        Constant.load(quoteEngine.env);
        quoteEngine.execute();
        break;
      case COMMAND_DAILY_PRICES:
        if (dailyPriceJob.help) {
          jCommander.usage(COMMAND_DAILY_PRICES);
          return;
        }
        Constant.load(dailyPriceJob.env);
        dailyPriceJob.execute();
        break;
      case COMMAND_DELETE_PRICES:
        if (deletePricesJob.help) {
          jCommander.usage(COMMAND_DELETE_PRICES);
          return;
        }
        Constant.load(deletePricesJob.env);
        deletePricesJob.execute();
        break;
      default:
        System.out.println("Unknown command");
        printCommands(jCommander);
        break;
    }
  }

  private static void printCommands(JCommander jCommander) {
    var commands = jCommander.getCommands();
    System.out.println("Usage: java -jar <jar-name>.jar <command> <options>");
    System.out.println("Available commands: ");
    for (var cmd : commands.keySet()) {
      System.out.println(
          String.format("      -%s: %s\n", cmd, jCommander.getCommandDescription(cmd)));
    }
    System.out.println(
        "Detailed instruction for each command: java -jar <jar-name>.jar <command> --help");
  }

  /**
   * API Doc generator: http://localhost:8081/swagger-ui.html
   */
  @Bean
  public Docket api() {
    return new Docket(DocumentationType.SWAGGER_2).select()
        .apis(RequestHandlerSelectors.basePackage("org.toxicaker.controllers")).build();
  }
}
