package org.toxicaker.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public final class DateUtil {

  /**
   * Convert Date object to date string
   *
   * @param date Date object
   * @return Eg: 2020-01-01
   */
  public static String dateToDateStr(Date date) {
    var df = new SimpleDateFormat("yyyy-MM-dd");
    return df.format(date);
  }

  /**
   * Get today's date string
   *
   * @return Eg: 2020-01-01
   */
  public static String today() {
    var df = new SimpleDateFormat("yyyy-MM-dd");
    return df.format(new Date());
  }

  /**
   * Get tomorrow's date string
   *
   * @param date today's date string. Eg: 2020-01-01
   */
  public static String tomorrow(String date) {
    var df = new SimpleDateFormat("yyyy-MM-dd");
    try {
      Date today = new Date(df.parse(date).getTime());
      Calendar cal = Calendar.getInstance();
      cal.setTime(today);
      cal.add(Calendar.DAY_OF_YEAR, 1);
      Date tomorrow = cal.getTime();
      return df.format(tomorrow);
    } catch (ParseException e) {
      return null;
    }
  }

  /**
   * Get tomorrow's date string
   *
   * @param date today's date string. Eg: 2020-01-01
   */
  public static String yesterday(String date) {
    var df = new SimpleDateFormat("yyyy-MM-dd");
    try {
      Date today = new Date(df.parse(date).getTime());
      Calendar cal = Calendar.getInstance();
      cal.setTime(today);
      cal.add(Calendar.DAY_OF_YEAR, -1);
      Date tomorrow = cal.getTime();
      return df.format(tomorrow);
    } catch (ParseException e) {
      return null;
    }
  }

  /**
   * Get today's date string. New York Time
   *
   * @return Eg: 2020-01-01
   */
  public static String todayEST() {
    var df = new SimpleDateFormat("yyyy-MM-dd");
    df.setTimeZone(TimeZone.getTimeZone("America/New_York"));
    return df.format(new Date());
  }

  /**
   * Transfer unix time to date string
   *
   * @return 2020-01-01 11:12:13
   */
  public static String unixToDateStr(long unix) {
    var df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    return df.format(new Date(unix));
  }

  /**
   * Get current time string of EST
   *
   * @return format: 2020-01-01 11:23:11
   */
  public static String getCurrentTimeEST() {
    var df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    df.setTimeZone(TimeZone.getTimeZone("America/New_York"));
    return df.format(new Date());
  }

  /**
   * Convert Date string to unit time
   *
   * @param date Format: 2011-01-01 22:11:10
   * @return unix milliseconds
   */
  public static long timeStrToUnix(String date) {
    var df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    try {
      return df.parse(date).getTime();
    } catch (ParseException e) {
      return 0;
    }
  }
}
