package org.toxicaker.utils;

import lombok.extern.slf4j.Slf4j;
import org.toxicaker.models.entities.ArkHoldEntity;
import org.toxicaker.models.entities.ArkTradeEntity;
import org.toxicaker.models.entities.BalanceSheetEntity;
import org.toxicaker.models.entities.CashFlowEntity;
import org.toxicaker.models.entities.CompanyProfileEntity;
import org.toxicaker.models.entities.IncomeStatementEntity;
import org.toxicaker.models.entities.NewsEntity;
import org.toxicaker.models.entities.PriceEntity;
import org.toxicaker.models.entities.QuoteEntity;
import org.toxicaker.models.services.ArkHold;
import org.toxicaker.models.services.ArkTrade;
import org.toxicaker.models.services.BalanceSheet;
import org.toxicaker.models.services.CashFlow;
import org.toxicaker.models.services.CompanyProfile;
import org.toxicaker.models.services.HistoricalPrice;
import org.toxicaker.models.services.IncomeStatement;
import org.toxicaker.models.services.News;
import org.toxicaker.models.services.Quote;

@Slf4j
public final class DataConvertUtil {

  public static ArkHold toArkHold(ArkHoldEntity arkHoldEntity) {
    var rst = transfer(ArkHoldEntity.class, ArkHold.class, arkHoldEntity);
    if (rst == null) {
      return null;
    } else {
      rst.setWeightChange(0.0);
      rst.setSharesChange(0.0);
      return rst;
    }
  }

  public static ArkTrade toArkTrade(ArkTradeEntity arkTradeEntity) {
    var rst = transfer(ArkTradeEntity.class, ArkTrade.class, arkTradeEntity);
    if (rst == null) {
      return null;
    } else {
      rst.setSharesChange(0.0);
      return rst;
    }
  }

  public static CompanyProfileEntity toCompanyProfileEntity(CompanyProfile companyProfile) {
    return transfer(CompanyProfile.class, CompanyProfileEntity.class, companyProfile);
  }

  public static CompanyProfile toCompanyProfile(CompanyProfileEntity companyProfileEntity) {
    return transfer(CompanyProfileEntity.class, CompanyProfile.class, companyProfileEntity);
  }

  public static News toNews(NewsEntity newsEntity) {
    return transfer(NewsEntity.class, News.class, newsEntity);
  }

  public static NewsEntity toNewsEntity(News news) {
    return transfer(News.class, NewsEntity.class, news);
  }

  public static IncomeStatementEntity toIncomeStatementEntity(IncomeStatement incomeStatement) {
    return transfer(IncomeStatement.class, IncomeStatementEntity.class, incomeStatement);
  }

  public static IncomeStatement toIncomeStatement(IncomeStatementEntity incomeStatementEntity) {
    return transfer(IncomeStatementEntity.class, IncomeStatement.class, incomeStatementEntity);
  }

  public static BalanceSheetEntity toBalanceSheetEntity(BalanceSheet balanceSheet) {
    return transfer(BalanceSheet.class, BalanceSheetEntity.class, balanceSheet);
  }

  public static BalanceSheet toBalanceSheet(BalanceSheetEntity balanceSheetEntity) {
    return transfer(BalanceSheetEntity.class, BalanceSheet.class, balanceSheetEntity);
  }

  public static CashFlowEntity toCashFlowEntity(CashFlow cashFlow) {
    return transfer(CashFlow.class, CashFlowEntity.class, cashFlow);
  }

  public static CashFlow toCashFlow(CashFlowEntity cashFlowEntity) {
    return transfer(CashFlowEntity.class, CashFlow.class, cashFlowEntity);
  }

  public static PriceEntity toPriceEntity(HistoricalPrice historicalPrice) {
    return transfer(HistoricalPrice.class, PriceEntity.class, historicalPrice);
  }

  public static HistoricalPrice toHistoricalPrice(PriceEntity priceEntity) {
    return transfer(PriceEntity.class, HistoricalPrice.class, priceEntity);
  }

  public static Quote toQuote(QuoteEntity quoteEntity) {
    return transfer(QuoteEntity.class, Quote.class, quoteEntity);
  }

  public static QuoteEntity toQuoteEntity(Quote quote) {
    return transfer(Quote.class, QuoteEntity.class, quote);
  }

  private static <T> T transfer(Class<?> inputClass, Class<? extends T> outputClass,
      Object inputObject) {
    try {
      var constructor = outputClass.getConstructors()[0];
      var rst = constructor.newInstance();
      var fields = inputClass.getDeclaredFields();
      for (var f : fields) {
        try {
          f.setAccessible(true);
          var name = f.getName();
          var val = f.get(inputObject);
          var targetField = outputClass.getDeclaredField(name);
          targetField.setAccessible(true);
          targetField.set(rst, val);
        } catch (NoSuchFieldException | IllegalAccessException ex) {
          // ignore
        }
      }
      return (T) rst;
    } catch (Exception e) {
      log.error("Failed to transfer object from {} to {}", inputClass, outputClass, e);
    }
    return null;
  }
}
