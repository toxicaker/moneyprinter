package org.toxicaker.utils;

import lombok.extern.slf4j.Slf4j;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.Protocol;

@Slf4j
public final class RedisUtil {

  private static final JedisPool POOL;

  private static Jedis jedis;

  public static Jedis getClient() {
    if (jedis == null) {
      jedis = POOL.getResource();
    }
    return jedis;
  }

  static {
    var host = "localhost";
    var port = 6379;
    POOL = new JedisPool(new JedisPoolConfig(), host, port, Protocol.DEFAULT_TIMEOUT);
    log.info("Connected with Redis: {}:{}", host, port);
  }
}
