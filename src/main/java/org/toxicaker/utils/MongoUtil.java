package org.toxicaker.utils;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoCredential;
import com.mongodb.ReadPreference;
import com.mongodb.ServerAddress;
import dev.morphia.Datastore;
import dev.morphia.Morphia;
import java.util.ArrayList;
import lombok.extern.slf4j.Slf4j;
import org.toxicaker.Main;
import org.toxicaker.common.Constant;

@Slf4j
public final class MongoUtil {

  private static Datastore datastore;
  private static final Morphia morphia = new Morphia();

  static {
    morphia.mapPackage("org.toxicaker.models.entities");
  }

  public static Datastore getDataStore() {
    if (datastore == null) {
      var mongoOptions = MongoClientOptions.builder()
          .connectTimeout(Constant.MONGO_DB_CONNECTION_TIMEOUT)
          .maxConnectionIdleTime(600000)
          .readPreference(ReadPreference.primaryPreferred())
          .build();
      var credentialsList = new ArrayList<MongoCredential>();
      if (!Constant.ENV.equals(Main.DEV)) {
        var cred = MongoCredential.createCredential(
            Constant.MONGO_DB_USERNAME, Constant.MONGO_DB_NAME,
            Constant.MONGO_DB_PASSWORD.toCharArray());
        credentialsList.add(cred);
        var mongoClient = new MongoClient(
            new ServerAddress(Constant.MONGO_DB_HOST, Constant.MONGO_DB_PORT), credentialsList,
            mongoOptions);
        datastore = morphia.createDatastore(mongoClient, Constant.MONGO_DB_NAME);
      } else {
        var mongoClient = new MongoClient(
            new ServerAddress(Constant.MONGO_DB_HOST, Constant.MONGO_DB_PORT), mongoOptions);
        datastore = morphia.createDatastore(mongoClient, Constant.MONGO_DB_NAME);
      }
      datastore.ensureIndexes();
      log.info("Connected with MongoDB: {}:{}/{}", Constant.MONGO_DB_HOST, Constant.MONGO_DB_PORT,
          Constant.MONGO_DB_NAME);
    }
    return datastore;
  }
}
