package org.toxicaker.controllers;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.toxicaker.common.JsonResp;
import org.toxicaker.services.StockService;

@RestController
@RequestMapping(value = "/stocks", produces = "application/json")
@CrossOrigin
public class StockController {

  private final StockService stockService;

  @Autowired
  public StockController(StockService stockService) {
    this.stockService = stockService;
  }

  @GetMapping("/quotes/{symbol}")
  @ApiOperation(value = "Get real-time stock quote metrics")
  public JsonResp getQuote(
      @PathVariable @ApiParam(value = "Stock symbol. Example: MSFT, NIO", required = true) String symbol) {
    return new JsonResp(stockService.getQuote(symbol));
  }

  @GetMapping("/prices/{symbol}")
  @ApiOperation(value = "List the daily prices of the given stock")
  public JsonResp listStockDailyPricesBySymbol(
      @PathVariable @ApiParam(value = "Stock symbol. Example: MSFT, NIO", required = true) String symbol,
      @RequestParam @ApiParam(value = "Date. Example: 2020-01-11", required = true) String from,
      @RequestParam @ApiParam(value = "Date. Example: 2020-01-11", required = true) String to) {
    return new JsonResp(stockService.listDailyPricesBySymbol(symbol, from, to));
  }

  @GetMapping("/search/symbols/{keyword}")
  @ApiOperation(value = "Search stock symbols")
  public JsonResp searchStockSymbols(
      @PathVariable @ApiParam(value = "The keyword to search stock symbols", required = true) String keyword) {
    return new JsonResp(stockService.searchSymbols(keyword));
  }
}
