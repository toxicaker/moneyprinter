package org.toxicaker.controllers;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.security.InvalidParameterException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.toxicaker.common.JsonResp;
import org.toxicaker.services.CompanyService;

@RestController
@RequestMapping(value = "/companies", produces = "application/json")
@CrossOrigin
public class CompanyController {

  private final CompanyService companyService;

  @Autowired
  public CompanyController(CompanyService companyService) {
    this.companyService = companyService;
  }


  @GetMapping("/profile/{symbol}")
  @ApiOperation(value = "Get company basic information")
  public JsonResp getCompanyProfile(
      @PathVariable @ApiParam(value = "Company symbol. Example: MSFT", required = true) String symbol) {
    return new JsonResp(companyService.getCompanyProfileBySymbol(symbol));
  }

  @GetMapping("/news/{symbol}")
  @ApiOperation(value = "The company news")
  public JsonResp listCompanyNews(
      @PathVariable @ApiParam(value = "Company symbol. Example: MSFT", required = true) String symbol,
      @RequestParam(defaultValue = "20") @ApiParam(value = "The number of news returned by the API") int limit) {
    return new JsonResp(companyService.listNewsBySymbol(symbol, limit));
  }

  @GetMapping("/sectors-industries")
  @ApiOperation(value = "List sectors and industries in the market")
  public JsonResp listSectorsAndIndustries() {
    return new JsonResp(companyService.listSectorsAndIndustries());
  }

  @GetMapping("/financial-reports/{symbol}")
  @ApiOperation(value = "List sectors and industries in the market")
  public JsonResp listFinancialReports(
      @PathVariable @ApiParam(value = "Company symbol. Example: MSFT", required = true) String symbol,
      @RequestParam @ApiParam(value = "Type. Income statement = ic. Balance sheet = bs. Cash Flow = cf", required = true) String type,
      @RequestParam(defaultValue = "true") @ApiParam(value = "The period of the financial statement. true = quarterly, false = annual. Default = true", required = true) boolean quarterly) {
    switch (type) {
      case "ic":
        return new JsonResp(companyService.listIncomeStatementsBySymbol(symbol, quarterly));
      case "bs":
        return new JsonResp(companyService.listBalanceSheetsBySymbol(symbol, quarterly));
      case "cf":
        return new JsonResp(companyService.listCashFlowsBySymbol(symbol, quarterly));
      default:
        throw new InvalidParameterException("{type} should be ic, bs or cf");
    }
  }
}
