package org.toxicaker.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.toxicaker.common.JsonResp;
import org.toxicaker.models.entities.ArkTradeEntity.OP;
import org.toxicaker.models.services.ArkTrade;
import org.toxicaker.services.ArkService;

@RestController
@RequestMapping(value = "/ark", produces = "application/json")
@Api(value = "ARK Investment Trading Data")
@CrossOrigin
public class ArkController {

  private final ArkService arkService;

  @Autowired
  public ArkController(ArkService arkService) {
    this.arkService = arkService;
  }

  @GetMapping("/holds/{symbol}")
  @ApiOperation(value = "List ARK stock position data by ARK EFT symbols and date")
  public JsonResp listArkHoldsBySymbolAndDate(
      @PathVariable @ApiParam(value = "ARK EFT symbol. Example: ARKK, ARKW", required = true) String symbol,
      @RequestParam @ApiParam(value = "Date. Example: 2020-01-11", required = true) String date) {
    return new JsonResp(arkService.listArkHoldsBySymbolAndDate(symbol, date));
  }

  @GetMapping("/trades/{symbol}")
  @ApiOperation(value = "List ARK trading operations by ARK EFT symbols and date")
  public JsonResp listArkTradesBySymbolAndDate(
      @PathVariable @ApiParam(value = "ARK EFT symbol. Example: ARKK, ARKW", required = true) String symbol,
      @RequestParam @ApiParam(value = "Date. Example: 2020-01-11", required = true) String date) {
    List<ArkTrade> buys = new ArrayList<>();
    List<ArkTrade> sells = new ArrayList<>();
    var trades = arkService.listArkTradesBySymbolAndDate(symbol, date);
    for (ArkTrade arkTrade : trades) {
      if (OP.BUY.val.equals(arkTrade.getOp())) {
        buys.add(arkTrade);
      } else {
        sells.add(arkTrade);
      }
    }
    buys.sort(Comparator.comparingDouble(i -> -Math.abs(i.getSharesChange())));
    sells.sort(Comparator.comparingDouble(i -> -Math.abs(i.getSharesChange())));
    return new JsonResp(Map.of("Buys", buys, "Sells", sells));
  }

  @GetMapping("/holds/{symbol}/{targetSymbol}")
  @ApiOperation(value = "List ARK stock position data for a specific stock")
  public JsonResp listArkHoldsBySymbolAndTargetSymbol(
      @PathVariable @ApiParam(value = "ARK EFT symbol. Example: ARKK, ARKW", required = true) String symbol,
      @PathVariable @ApiParam(value = "Company symbol. Example: NIO, MSFT", required = true) String targetSymbol,
      @RequestParam @ApiParam(value = "Date. Example: 2020-01-11", required = true) String from,
      @RequestParam @ApiParam(value = "Date. Example: 2020-01-11", required = true) String to) {
    return new JsonResp(arkService.
        listArkHoldsBySymbolAndTargetSymbol(symbol, targetSymbol, from, to));
  }

  @GetMapping("/trades/{symbol}/{targetSymbol}")
  @ApiOperation(value = "List ARK trading operation data for a specific stock")
  public JsonResp listArkTradesBySymbolAndTargetSymbol(
      @PathVariable @ApiParam(value = "ARK EFT symbol. Example: ARKK, ARKW", required = true) String symbol,
      @PathVariable @ApiParam(value = "Company symbol. Example: NIO, MSFT", required = true) String targetSymbol,
      @RequestParam @ApiParam(value = "Date. Example: 2020-01-11", required = true) String from,
      @RequestParam @ApiParam(value = "Date. Example: 2020-01-11", required = true) String to) {
    return new JsonResp(arkService.
        listArkTradesBySymbolAndTargetSymbol(symbol, targetSymbol, from, to));
  }

  @GetMapping("/available-date/{type}")
  @ApiOperation(value = "List the dates that have ARK trading and stock position data")
  public JsonResp listAvailableDates(
      @PathVariable @ApiParam(value = "Type of available dates. Example: holding, trade", required = true) String type) {
    if ("trade".equals(type)) {
      return new JsonResp(arkService.listArkTradesAvailableDates());
    }
    return new JsonResp(arkService.listArkHoldingsAvailableDates());
  }

  @GetMapping("/available-efts")
  @ApiOperation(value = "List the all the available symbols of ARK series")
  public JsonResp listARKSymbols() {
    return new JsonResp(List.of("ARKK", "ARKQ", "ARKF", "ARKW", "ARKG", "PRNT"));
  }
}
