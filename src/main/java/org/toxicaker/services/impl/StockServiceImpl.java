package org.toxicaker.services.impl;

import com.alibaba.fastjson.JSON;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.toxicaker.dal.mongo.PriceEntityDao;
import org.toxicaker.dal.mongo.QuoteEntityDao;
import org.toxicaker.models.services.HistoricalPrice;
import org.toxicaker.models.services.Quote;
import org.toxicaker.services.StockService;
import org.toxicaker.utils.DataConvertUtil;
import org.toxicaker.utils.RedisUtil;

@Service
public class StockServiceImpl implements StockService {

  private final QuoteEntityDao quoteEntityDao;
  private final PriceEntityDao priceEntityDao;

  @Autowired
  public StockServiceImpl(QuoteEntityDao quoteEntityDao,
      PriceEntityDao priceEntityDao) {
    this.quoteEntityDao = quoteEntityDao;
    this.priceEntityDao = priceEntityDao;
  }

  @Override
  public void saveRealTimeQuote(Quote quote) {
    var key = "quote-" + quote.getSymbol();
    RedisUtil.getClient().set(key, JSON.toJSONString(quote));
  }

  @Override
  public Quote getRealTimeQuote(String symbol) {
    var key = "quote-" + symbol;
    var jsonStr = RedisUtil.getClient().get(key);
    if (StringUtils.isEmpty(jsonStr)) {
      return null;
    }
    return JSON.parseObject(jsonStr, Quote.class);
  }

  @Override
  public void saveRealTimePrice(String symbol, Double price) {
    var key = "price-" + symbol;
    RedisUtil.getClient().set(key, price + "");
  }

  @Override
  public Double getRealTimePrice(String symbol) {
    var key = "price-" + symbol;
    var jsonStr = RedisUtil.getClient().get(key);
    if (StringUtils.isEmpty(jsonStr)) {
      return null;
    }
    return Double.parseDouble(jsonStr);
  }

  @Override
  public Quote getQuote(String symbol) {
    return DataConvertUtil.toQuote(quoteEntityDao.getQuoteEntityBySymbol(symbol));
  }

  @Override
  public List<HistoricalPrice> listDailyPricesBySymbol(String symbol, String from, String to) {
    return priceEntityDao.listPriceEntities(symbol, from, to)
        .stream()
        .map(DataConvertUtil::toHistoricalPrice)
        .collect(Collectors.toList());
  }

  @Override
  public List<String> searchSymbols(String keyword) {
    var symbols = quoteEntityDao.searchSymbols(keyword);
    symbols.sort(Comparator.comparing(String::length));
    return symbols;
  }
}
