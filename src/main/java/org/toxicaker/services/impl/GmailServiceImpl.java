package org.toxicaker.services.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.search.SubjectTerm;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.toxicaker.common.Constant;
import org.toxicaker.services.GmailService;

@Service
@Slf4j
public class GmailServiceImpl implements GmailService {

  private Store store;

  public GmailServiceImpl() {

  }

  @Override
  public List<Message> listEmailsBySubject(String... subjects) throws MessagingException {
    var emailFolder = getStore().getFolder("INBOX");
    emailFolder.open(Folder.READ_ONLY);
    var rst = new ArrayList<Message>();
    for (var sub : subjects) {
      var searchTerm = new SubjectTerm(sub);
      rst.addAll(Arrays.asList(emailFolder.search(searchTerm)));
    }
    return rst;
  }

  private Store getStore() throws MessagingException {
    if (store == null) {
      var properties = new Properties();
      properties.setProperty("mail.imap.ssl.enable", "true");
      var emailSession = Session.getDefaultInstance(properties);
      store = emailSession.getStore("imap");
      store.connect(Constant.EMAIL_HOST, Constant.EMAIL_USERNAME, Constant.EMAIL_PASSWORD);
    }
    return store;
  }
}
