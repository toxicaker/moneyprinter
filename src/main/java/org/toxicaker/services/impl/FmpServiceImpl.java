package org.toxicaker.services.impl;

import com.alibaba.fastjson.JSON;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import lombok.Data;
import org.springframework.stereotype.Service;
import org.toxicaker.common.Constant;
import org.toxicaker.models.services.BalanceSheet;
import org.toxicaker.models.services.CashFlow;
import org.toxicaker.models.services.CompanyProfile;
import org.toxicaker.models.services.HistoricalPrice;
import org.toxicaker.models.services.IncomeStatement;
import org.toxicaker.models.services.News;
import org.toxicaker.models.services.Quote;
import org.toxicaker.models.services.Symbol;
import org.toxicaker.services.FmpService;

@Service
public class FmpServiceImpl implements FmpService {

  private static final String HOST = "https://fmpcloud.io/api";

  private final OkHttpClient httpClient = new OkHttpClient();

  @Override
  public List<Quote> quotePrices(String... symbols) throws IOException {
    var url = String.format("%s/v3/quote/%s?apikey=%s",
        HOST, String.join(",", symbols), Constant.FMP_KEY);
    return JSON.parseArray(handleRequest(url).body().string(), Quote.class);
  }


  @Data
  private static class HistoricalPriceResp {
    private String symbol;
    private List<HistoricalPrice> historical = new ArrayList<>();
  }

  @Override
  public List<HistoricalPrice> listDailyPrice(String symbol, String from, String to)
      throws IOException {
    var url = String.format("%s/v3/historical-price-full/%s?from=%s&to=%s&apikey=%s",
        HOST, symbol, from, to, Constant.FMP_KEY);
    var prices = JSON.parseObject(handleRequest(url).body().string(), HistoricalPriceResp.class);
    prices.getHistorical().forEach(i -> i.setSymbol(symbol));
    return prices.getHistorical();
  }

  @Override
  public List<Symbol> listSymbols() throws IOException {
    var url = String.format("%s/v3/stock/list?apikey=%s", HOST, Constant.FMP_KEY);
    return JSON.parseArray(handleRequest(url).body().string(), Symbol.class)
        .stream()
        .filter(i -> !i.getSymbol().contains("."))
        .collect(Collectors.toList());
  }

  @Override
  public List<String> listSymbolsWithFinancialReports() throws IOException {
    var url = String
        .format("%s/v3/financial-statement-symbol-lists?apikey=%s", HOST, Constant.FMP_KEY);
    return JSON.parseArray(handleRequest(url).body().string(), String.class).stream()
        .filter(s -> !s.contains(".")).collect(Collectors.toList());
  }

  @Override
  public List<CompanyProfile> listCompanyProfiles(List<String> symbols) throws IOException {
    var url = String.format("%s/v3/profile/%s?apikey=%s",
        HOST, String.join(",", symbols), Constant.FMP_KEY);
    return JSON.parseArray(handleRequest(url).body().string(), CompanyProfile.class);
  }

  @Override
  public List<News> listNews(String... symbols) throws IOException {
    var url = String.format("%s/v3/stock_news?tickers=%s&limit=100&apikey=%s",
        HOST, String.join(",", symbols), Constant.FMP_KEY);
    return JSON.parseArray(handleRequest(url).body().string(), News.class);
  }

  @Override
  public List<IncomeStatement> listIncomeStatements(String symbol, boolean quarterly, int limit)
      throws IOException {
    var url = quarterly ? String.format("%s/v3/income-statement/%s?limit=%d&apikey=%s",
        HOST, symbol, limit, Constant.FMP_KEY) :
        String.format("%s/v3/income-statement/%s?period=quarter&limit=%d&apikey=%s",
            HOST, symbol, limit, Constant.FMP_KEY);
    return JSON.parseArray(handleRequest(url).body().string(), IncomeStatement.class);
  }

  @Override
  public List<BalanceSheet> listBalanceSheets(String symbol, boolean quarterly, int limit)
      throws IOException {
    var url = quarterly ? String.format("%s/v3/balance-sheet-statement/%s?limit=%d&apikey=%s",
        HOST, symbol, limit, Constant.FMP_KEY) :
        String.format("%s/v3/balance-sheet-statement/%s?period=quarter&limit=%d&apikey=%s",
            HOST, symbol, limit, Constant.FMP_KEY);
    return JSON.parseArray(handleRequest(url).body().string(), BalanceSheet.class);
  }

  @Override
  public List<CashFlow> listCashFlows(String symbol, boolean quarterly, int limit)
      throws IOException {
    var url = quarterly ? String.format("%s/v3/cash-flow-statement/%s?limit=%d&apikey=%s",
        HOST, symbol, limit, Constant.FMP_KEY) :
        String.format("%s/v3/cash-flow-statement/%s?period=quarter&limit=%d&apikey=%s",
            HOST, symbol, limit, Constant.FMP_KEY);
    return JSON.parseArray(handleRequest(url).body().string(), CashFlow.class);
  }

  private Response handleRequest(String url) throws IOException {
    var request = new Request.Builder().url(url).get().build();
    return httpClient.newCall(request).execute();
  }
}
