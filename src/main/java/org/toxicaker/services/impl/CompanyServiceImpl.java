package org.toxicaker.services.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.toxicaker.dal.mongo.CompanyDao;
import org.toxicaker.models.services.BalanceSheet;
import org.toxicaker.models.services.CashFlow;
import org.toxicaker.models.services.CompanyProfile;
import org.toxicaker.models.services.IncomeStatement;
import org.toxicaker.models.services.News;
import org.toxicaker.services.CompanyService;
import org.toxicaker.utils.DataConvertUtil;

/**
 * Company basic information, financial reports, financial metrics.
 */

@Service
public class CompanyServiceImpl implements CompanyService {


  private final CompanyDao companyDao;

  @Autowired
  public CompanyServiceImpl(CompanyDao companyDao) {
    this.companyDao = companyDao;
  }

  @Override
  public CompanyProfile getCompanyProfileBySymbol(String symbol) {
    return DataConvertUtil
        .toCompanyProfile(companyDao.getCompanyProfileEntityBySymbol(symbol));
  }

  @Override
  public Map<String, List<String>> listSectorsAndIndustries() {
    var sectors = companyDao.listSectors();
    var rst = new TreeMap<String, List<String>>();
    for (var sector : sectors) {
      var industries = new ArrayList<>(companyDao.listIndustriesBySector(sector));
      Collections.sort(industries);
      rst.put(sector, industries);
    }
    return rst;
  }

  @Override
  public List<News> listNewsBySymbol(String symbol, int limit) {
    return companyDao.listNewsEntitiesBySymbolOrderedByDateDesc(symbol, limit)
        .stream()
        .map(DataConvertUtil::toNews)
        .collect(Collectors.toList());
  }

  @Override
  public List<IncomeStatement> listIncomeStatementsBySymbol(String symbol, boolean quarterly) {
    return companyDao.listIncomeStatementEntitiesBySymbol(symbol, quarterly).stream().map(
        DataConvertUtil::toIncomeStatement).collect(Collectors.toList());
  }

  @Override
  public List<BalanceSheet> listBalanceSheetsBySymbol(String symbol, boolean quarterly) {
    return companyDao.listBalanceSheetEntitiesBySymbol(symbol, quarterly).stream().map(
        DataConvertUtil::toBalanceSheet).collect(Collectors.toList());
  }

  @Override
  public List<CashFlow> listCashFlowsBySymbol(String symbol, boolean quarterly) {
    return companyDao.listCashFlowEntitiesBySymbol(symbol, quarterly).stream().map(
        DataConvertUtil::toCashFlow).collect(Collectors.toList());
  }
}
