package org.toxicaker.services.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.toxicaker.dal.mongo.ArkEntityDao;
import org.toxicaker.models.entities.ArkHoldEntity;
import org.toxicaker.models.entities.ArkTradeEntity;
import org.toxicaker.models.entities.ArkTradeEntity.OP;
import org.toxicaker.models.services.ArkHold;
import org.toxicaker.models.services.ArkTrade;
import org.toxicaker.services.ArkService;
import org.toxicaker.utils.DataConvertUtil;

@Service
public class ArkServiceImpl implements ArkService {

  private final ArkEntityDao arkEntityDao;

  @Autowired
  public ArkServiceImpl(ArkEntityDao arkEntityDao) {
    this.arkEntityDao = arkEntityDao;
  }

  @Override
  public List<ArkTrade> listArkTradesBySymbolAndDate(String symbol, String date) {
    var trades = arkEntityDao.listArkTradesBySymbolAndDate(symbol, date);
    var prevDate = arkEntityDao.getArkHoldLatestDateBefore(date);
    List<ArkTrade> rst = new ArrayList<>();

    for (ArkTradeEntity t : trades) {
      ArkTrade arkTrade = DataConvertUtil.toArkTrade(t);
      rst.add(arkTrade);
    }

    if (prevDate != null) {
      var holds = arkEntityDao.listArkHoldsBySymbolAndDate(symbol, prevDate);
      Map<String, Integer> shareMap = new HashMap<>();
      for (ArkHoldEntity h : holds) {
        shareMap.put(h.getTargetSymbol(), h.getShares());
      }
      for (ArkTrade arkTrade : rst) {
        if (shareMap.containsKey(arkTrade.getTargetSymbol())) {
          var change = arkTrade.getOp().equals(OP.BUY.val) ? arkTrade.getShares()
              / (double) shareMap.get(arkTrade.getTargetSymbol()) :
              -arkTrade.getShares() / (double) shareMap.get(arkTrade.getTargetSymbol());
          arkTrade.setSharesChange(change);
        }
      }
    }
    return rst;
  }

  @Override
  public List<ArkTrade> listArkTradesBySymbolAndTargetSymbol(String symbol, String targetSymbol,
      String from, String to) {
    var trades = arkEntityDao.listArkTradesBySymbolAndTargetSymbol(symbol, targetSymbol, from, to);
    List<ArkTrade> rst = new ArrayList<>();

    for (ArkTradeEntity t : trades) {
      ArkTrade arkTrade = DataConvertUtil.toArkTrade(t);
      var prevDate = arkEntityDao.getArkHoldLatestDateBefore(t.getDate());
      if (prevDate != null) {
        var prevHold = arkEntityDao
            .getArkHoldBySymbolAndTargetSymbolAndDate(symbol, arkTrade.getTargetSymbol(), prevDate);
        if (prevHold != null) {
          var change = arkTrade.getOp().equals(OP.BUY.val) ? arkTrade.getShares()
              / (double) prevHold.getShares() :
              -arkTrade.getShares() / (double) prevHold.getShares();
          arkTrade.setSharesChange(change);
        }
      }
      rst.add(arkTrade);
    }
    return rst;
  }

  @Override
  public List<ArkHold> listArkHoldsBySymbolAndDate(String symbol, String date) {
    var holds = arkEntityDao.listArkHoldsBySymbolAndDate(symbol, date);
    var prevDate = arkEntityDao.getArkHoldLatestDateBefore(date);
    List<ArkHold> rst = new ArrayList<>();

    for (ArkHoldEntity h : holds) {
      ArkHold arkHold = DataConvertUtil.toArkHold(h);
      rst.add(arkHold);
    }

    if (prevDate != null) {
      var prevHolds = arkEntityDao.listArkHoldsBySymbolAndDate(symbol, prevDate);
      Map<String, ArkHoldEntity> holdMap = new HashMap<>();
      for (ArkHoldEntity h : prevHolds) {
        holdMap.put(h.getTargetSymbol(), h);
      }
      for (ArkHold hold : rst) {
        if (holdMap.containsKey(hold.getTargetSymbol())) {
          hold.setSharesChange((hold.getShares() - holdMap.get(hold.getTargetSymbol()).getShares())
              / (double) holdMap.get(hold.getTargetSymbol()).getShares());
          hold.setWeightChange((hold.getWeight() - holdMap.get(hold.getTargetSymbol()).getWeight())
              / holdMap.get(hold.getTargetSymbol()).getWeight());
        }
      }
    }
    return rst;
  }


  @Override
  public List<ArkHold> listArkHoldsBySymbolAndTargetSymbol(String symbol, String targetSymbol,
      String from, String to) {
    var holds = arkEntityDao.listArkHoldsBySymbolAndTargetSymbol(symbol, targetSymbol, from, to);
    List<ArkHold> rst = new ArrayList<>();

    for (ArkHoldEntity h : holds) {
      ArkHold arkHold = DataConvertUtil.toArkHold(h);
      var prevDate = arkEntityDao.getArkHoldLatestDateBefore(h.getDate());
      if (prevDate != null) {
        var prevHold = arkEntityDao
            .getArkHoldBySymbolAndTargetSymbolAndDate(symbol, arkHold.getTargetSymbol(), prevDate);
        if (prevHold != null) {
          arkHold.setSharesChange((arkHold.getShares() - prevHold.getShares())
              / (double) prevHold.getShares());
          arkHold.setWeightChange((arkHold.getWeight() - prevHold.getWeight())
              / prevHold.getWeight());
        }
      }
      rst.add(arkHold);
    }
    return rst;
  }

  @Override
  public List<String> listArkHoldingsAvailableDates() {
    return arkEntityDao.listArkHoldsAvailableDate();
  }

  @Override
  public List<String> listArkTradesAvailableDates() {
    return arkEntityDao.listArkTradesAvailableDate();
  }
}
