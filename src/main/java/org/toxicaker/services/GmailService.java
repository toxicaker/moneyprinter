package org.toxicaker.services;

import java.io.IOException;
import java.util.List;
import javax.mail.Message;
import javax.mail.MessagingException;

public interface GmailService {

  /**
   * Search emails
   *
   * @param subjects Email subject
   */
  List<Message> listEmailsBySubject(String... subjects) throws IOException, MessagingException;
}
