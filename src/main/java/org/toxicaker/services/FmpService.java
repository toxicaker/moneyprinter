package org.toxicaker.services;

import java.io.IOException;
import java.util.List;
import org.toxicaker.models.services.BalanceSheet;
import org.toxicaker.models.services.CashFlow;
import org.toxicaker.models.services.CompanyProfile;
import org.toxicaker.models.services.HistoricalPrice;
import org.toxicaker.models.services.IncomeStatement;
import org.toxicaker.models.services.News;
import org.toxicaker.models.services.Quote;
import org.toxicaker.models.services.Symbol;

/**
 * Stock data provider: https://fmpcloud.io/
 */
public interface FmpService {

  /**
   * Quote real-time prices
   *
   * @param symbols symbol array
   */
  List<Quote> quotePrices(String... symbols) throws IOException;

  /**
   * List historical daily prices
   */
  List<HistoricalPrice> listDailyPrice(String symbol, String from, String to) throws IOException;

  /**
   * List all symbols
   */
  List<Symbol> listSymbols() throws IOException;

  /**
   * List all symbols that have financial report
   */
  List<String> listSymbolsWithFinancialReports() throws IOException;

  /**
   * List company basic information
   */
  List<CompanyProfile> listCompanyProfiles(List<String> symbols) throws IOException;

  /**
   * List the news of the companies
   */
  List<News> listNews(String... symbols) throws IOException;

  /**
   * List income statements of the company
   *
   * @param quarterly annual or quarterly income statements
   */
  List<IncomeStatement> listIncomeStatements(String symbol, boolean quarterly, int limit) throws IOException;

  /**
   * List balance sheets of the company
   *
   * @param quarterly annual or quarterly income statements
   */
  List<BalanceSheet> listBalanceSheets(String symbol, boolean quarterly, int limit) throws IOException;

  /**
   * List cash flows of the company
   *
   * @param quarterly annual or quarterly income statements
   */
  List<CashFlow> listCashFlows(String symbol, boolean quarterly, int limit) throws IOException;
}
