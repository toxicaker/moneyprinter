package org.toxicaker.services;

import java.util.List;
import org.toxicaker.models.services.ArkHold;
import org.toxicaker.models.services.ArkTrade;

public interface ArkService {

  /**
   * List ARK trading records
   *
   * @param symbol EFT symbol: ARKK, ARKG......
   * @param date   Eg: 2020-01-01
   */
  List<ArkTrade> listArkTradesBySymbolAndDate(String symbol, String date);

  /**
   * List ARK trading records for a specific symbol
   *
   * @param symbol       EFT symbol: ARKK, ARKG......
   * @param targetSymbol Company symbol
   * @param from         Start date
   * @param to           End date
   */
  List<ArkTrade> listArkTradesBySymbolAndTargetSymbol(String symbol, String targetSymbol, String from,
      String to);

  /**
   * List ARK holding stocks
   *
   * @param symbol EFT symbol: ARKK, ARKG......
   * @param date   Eg: 2020-01-01
   */
  List<ArkHold> listArkHoldsBySymbolAndDate(String symbol, String date);

  /**
   * List ARK holding stocks for a specific symbol
   *
   * @param symbol       EFT symbol: ARKK, ARKG......
   * @param targetSymbol Company symbol
   * @param from         Start date
   * @param to           End date
   */
  List<ArkHold> listArkHoldsBySymbolAndTargetSymbol(String symbol, String targetSymbol, String from,
      String to);

  /**
   * List available ARK holding dates saved in database
   *
   * @return List of date string
   */
  List<String> listArkHoldingsAvailableDates();

  /**
   * List available ARK trading dates saved in database
   *
   * @return List of date string
   */
  List<String> listArkTradesAvailableDates();
}
