package org.toxicaker.services;

import java.util.List;
import org.toxicaker.models.services.HistoricalPrice;
import org.toxicaker.models.services.Quote;

public interface StockService {

  /**
   * Save real-time quote into Redis. Redis key: quote-{symbol}, val: {quote json string}.
   */
  void saveRealTimeQuote(Quote quote);

  /**
   * Get real-time quote data from Redis
   */
  Quote getRealTimeQuote(String symbol);

  /**
   * Save real-time price into Redis. Redis key: price-{symbol}, val: {price}. This data is more
   * accurate than "quote" object.
   */
  void saveRealTimePrice(String symbol, Double price);

  /**
   * Get real-time price from Redis
   */
  Double getRealTimePrice(String symbol);

  /**
   * Quote stock metrics
   */
  Quote getQuote(String symbol);

  /**
   * list daily prices of given symbol
   *
   * @param from start date: 2021-01-01
   * @param to   end date: 2022-02-20
   */
  List<HistoricalPrice> listDailyPricesBySymbol(String symbol, String from, String to);

  /**
   * Search stock symbols based on keyword
   */
  List<String> searchSymbols(String keyword);
}
