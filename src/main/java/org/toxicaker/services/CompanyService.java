package org.toxicaker.services;

import java.util.List;
import java.util.Map;
import org.toxicaker.models.services.BalanceSheet;
import org.toxicaker.models.services.CashFlow;
import org.toxicaker.models.services.CompanyProfile;
import org.toxicaker.models.services.IncomeStatement;
import org.toxicaker.models.services.News;

/**
 * Company basic information, financial reports, financial metrics.
 */
public interface CompanyService {

  /**
   * Get company profile
   *
   * @return null if not exists
   */
  CompanyProfile getCompanyProfileBySymbol(String symbol);

  /**
   * List the sectors and industries in the market
   *
   * @return {sector -> industries} ordered by alphabet ascending
   */
  Map<String, List<String>> listSectorsAndIndustries();

  /**
   * List the most recent news by symbol
   */
  List<News> listNewsBySymbol(String symbol, int limit);

  /**
   * List all income statements by symbol
   * @param quarterly = true: list Q1, Q2, Q3, Q4, false: FY
   */
  List<IncomeStatement> listIncomeStatementsBySymbol(String symbol, boolean quarterly);

  /**
   * List all balance sheets by symbol
   * @param quarterly = true: list Q1, Q2, Q3, Q4, false: FY
   */
  List<BalanceSheet> listBalanceSheetsBySymbol(String symbol, boolean quarterly);

  /**
   * List all cash flow statements by symbol
   * @param quarterly = true: list Q1, Q2, Q3, Q4, false: FY
   */
  List<CashFlow> listCashFlowsBySymbol(String symbol, boolean quarterly);
}
