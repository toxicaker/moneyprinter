package org.toxicaker.models.services;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

@Data
@Builder
public class CashFlow {

  private String date;
  private String symbol;
  private String reportedCurrency;
  private String fillingDate;
  private String acceptedDate;
  private String period;
  private Double netIncome;
  private Double depreciationAndAmortization;
  private Double deferredIncomeTax;
  private Double stockBasedCompensation;
  private Double changeInWorkingCapital;
  private Double accountsReceivables;
  private Double inventory;
  private Double accountsPayables;
  private Double otherWorkingCapital;
  private Double otherNonCashItems;
  private Double netCashProvidedByOperatingActivities;
  private Double investmentsInPropertyPlantAndEquipment;
  private Double acquisitionsNet;
  private Double purchasesOfInvestments;
  private Double salesMaturitiesOfInvestments;
  private Double otherInvestingActivites;
  private Double netCashUsedForInvestingActivites;
  private Double debtRepayment;
  private Double commonStockIssued;
  private Double commonStockRepurchased;
  private Double dividendsPaid;
  private Double otherFinancingActivites;
  private Double netCashUsedProvidedByFinancingActivities;
  private Double effectOfForexChangesOnCash;
  private Double netChangeInCash;
  private Double cashAtEndOfPeriod;
  private Double cashAtBeginningOfPeriod;
  private Double operatingCashFlow;
  private Double capitalExpenditure;
  private Double freeCashFlow;

  @Tolerate
  public CashFlow() {

  }
}
