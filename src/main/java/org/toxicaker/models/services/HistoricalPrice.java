package org.toxicaker.models.services;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

@Data
@Builder
public class HistoricalPrice {

  private String date;
  private String symbol;
  private Double open;
  private Double high;
  private Double low;
  private Double close;
  private Long volume;
  private Double change;
  private Double changePercent;

  @Tolerate
  public HistoricalPrice() {

  }
}
