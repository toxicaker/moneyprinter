package org.toxicaker.models.services;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

@Data
@Builder
public class ArkHold {

  private String symbol;
  private String company;
  private String date;
  private String targetSymbol;
  private Double marketValue;
  private Integer shares;
  private Double weight;
  private Double sharesChange;  // share number change compared with the last trading date
  private Double weightChange;  // weight change compared with the last trading date

  @Tolerate
  public ArkHold() {

  }
}
