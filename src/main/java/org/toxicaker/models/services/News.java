package org.toxicaker.models.services;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

@Data
@Builder
public class News {

  private String symbol;
  private String publishedDate;
  private String title;
  private String image;
  private String site;
  private String text;
  private String url;

  @Tolerate
  public News() {

  }
}
