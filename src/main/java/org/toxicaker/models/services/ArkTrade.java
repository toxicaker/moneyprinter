package org.toxicaker.models.services;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

@Data
@Builder
public class ArkTrade {

  private String symbol;  // ETF symbol
  private String date;
  private String targetSymbol; // operation target
  private String company;
  private String op;
  private Integer shares;
  private Double weight;
  private Double sharesChange; // change percent compared with the last trading day

  @Tolerate
  public ArkTrade() {

  }
}
