package org.toxicaker.models.services;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

@Data
@Builder
public class BalanceSheet {
  private String date;
  private String symbol;
  private String reportedCurrency;
  private String fillingDate;
  private String acceptedDate;
  private String period;
  private Double cashAndCashEquivalents;
  private Double shortTermInvestments;
  private Double cashAndShortTermInvestments;
  private Double netReceivables;
  private Double inventory;
  private Double otherCurrentAssets;
  private Double totalCurrentAssets;
  private Double propertyPlantEquipmentNet;
  private Double goodwill;
  private Double intangibleAssets;
  private Double goodwillAndIntangibleAssets;
  private Double longTermInvestments;
  private Double taxAssets;
  private Double otherNonCurrentAssets;
  private Double totalNonCurrentAssets;
  private Double otherAssets;
  private Double totalAssets;
  private Double accountPayables;
  private Double shortTermDebt;
  private Double taxPayables;
  private Double deferredRevenue;
  private Double otherCurrentLiabilities;
  private Double totalCurrentLiabilities;
  private Double longTermDebt;
  private Double deferredRevenueNonCurrent;
  private Double deferredTaxLiabilitiesNonCurrent;
  private Double otherNonCurrentLiabilities;
  private Double totalNonCurrentLiabilities;
  private Double otherLiabilities;
  private Double totalLiabilities;
  private Long commonStock;
  private Double retainedEarnings;
  private Double accumulatedOtherComprehensiveIncomeLoss;
  private Double othertotalStockholdersEquity;
  private Double totalStockholdersEquity;
  private Double totalLiabilitiesAndStockholdersEquity;
  private Double totalInvestments;
  private Double totalDebt;
  private Double netDebt;

  @Tolerate
  public BalanceSheet() {

  }
}
