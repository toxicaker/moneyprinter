package org.toxicaker.models.services;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

@Data
@Builder
public class Symbol {

  private String symbol;
  private String name;

  @Tolerate
  public Symbol() {

  }
}

