package org.toxicaker.models.entities;

import dev.morphia.annotations.Entity;
import dev.morphia.annotations.Id;
import dev.morphia.annotations.Property;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import org.bson.types.ObjectId;

@Entity("fund_operations")
@Data
@Builder
public class ArkTradeEntity {

  @Id
  private ObjectId id;
  private String symbol;  // ETF symbol
  private String date;
  @Property("target_symbol")
  private String targetSymbol; // operation target
  private String company;
  private String op;
  private Integer shares;
  private Double weight;

  @Tolerate
  public ArkTradeEntity() {

  }

  public enum OP {
    BUY("Buy"),
    SELL("Sell");
    public String val;

    OP(String op) {
      this.val = op;
    }
  }
}
