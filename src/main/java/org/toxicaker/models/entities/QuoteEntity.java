package org.toxicaker.models.entities;

import dev.morphia.annotations.Entity;
import dev.morphia.annotations.Id;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import org.bson.types.ObjectId;

@Entity("quotes")
@Data
@Builder
public class QuoteEntity {

  @Id
  private ObjectId id;
  private String symbol;
  private Double price;
  private Double changesPercentage;
  private Double change;
  private Double dayLow;
  private Double dayHigh;
  private Double yearLow;
  private Double yearHigh;
  private Double marketCap;
  private Long volume;
  private Long avgVolume;
  private String exchange;
  private Double open;
  private Double previousClose;
  private Long sharesOutstanding;
  private Long timestamp;
  private Double eps;
  private Double pe;

  @Tolerate
  public QuoteEntity() {

  }
}
