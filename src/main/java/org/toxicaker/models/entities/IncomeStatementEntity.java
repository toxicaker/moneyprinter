package org.toxicaker.models.entities;

import dev.morphia.annotations.Entity;
import dev.morphia.annotations.Id;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import org.bson.types.ObjectId;

@Data
@Builder
@Entity("incomeStatement")
public class IncomeStatementEntity {

  @Id
  private ObjectId id;
  private String date;
  private String symbol;
  private String reportedCurrency;
  private String fillingDate;
  private String acceptedDate;
  private String period;
  private Double revenue;
  private Double costOfRevenue;
  private Double grossProfit;
  private Double grossProfitRatio;
  private Double researchAndDevelopmentExpenses;
  private Double generalAndAdministrativeExpenses;
  private Double sellingAndMarketingExpenses;
  private Double otherExpenses;
  private Double operatingExpenses;
  private Double costAndExpenses;
  private Double interestExpense;
  private Double depreciationAndAmortization;
  private Double ebitda;
  private Double ebitdaratio;
  private Double operatingIncome;
  private Double operatingIncomeRatio;
  private Double totalOtherIncomeExpensesNet;
  private Double incomeBeforeTax;
  private Double incomeBeforeTaxRatio;
  private Double incomeTaxExpense;
  private Double netIncome;
  private Double netIncomeRatio;
  private Double eps;
  private Double epsdiluted;
  public Long weightedAverageShsOut;
  public Long weightedAverageShsOutDil;

  @Tolerate
  public IncomeStatementEntity() {

  }
}
