package org.toxicaker.models.entities;

import dev.morphia.annotations.Entity;
import dev.morphia.annotations.Id;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import org.bson.types.ObjectId;

@Entity("companyProfiles")
@Data
@Builder
public class CompanyProfileEntity {

  @Id
  private ObjectId id;
  private String symbol;
  private Double price;
  private Double beta;
  private Long volAvg;
  private Long mktCap;
  private Double lastDiv;
  private String range;
  private Double changes;
  private String companyName;
  private String currency;
  private String cik;
  private String isin;
  private String cusip;
  private String exchange;
  private String exchangeShortName;
  private String industry;
  private String website;
  private String description;
  private String ceo;
  private String sector;
  private String country;
  private String fullTimeEmployees;
  private String phone;
  private String address;
  private String city;
  private String state;
  private String zip;
  private Double dcfDiff;
  private Double dcf;
  private String image;
  private String ipoDate;
  private Boolean defaultImage;
  private Boolean isEtf;
  private Boolean isActivelyTrading;

  @Tolerate
  public CompanyProfileEntity() {
  }
}
