package org.toxicaker.models.entities;

import dev.morphia.annotations.Entity;
import dev.morphia.annotations.Id;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import org.bson.types.ObjectId;

@Data
@Builder
@Entity("balanceSheet")
public class BalanceSheetEntity {
  @Id
  private ObjectId id;
  private String date;
  private String symbol;
  private String reportedCurrency;
  private String fillingDate;
  private String acceptedDate;
  private String period;
  private Double cashAndCashEquivalents;
  private Double shortTermInvestments;
  private Double cashAndShortTermInvestments;
  private Double netReceivables;
  private Double inventory;
  private Double otherCurrentAssets;
  private Double totalCurrentAssets;
  private Double propertyPlantEquipmentNet;
  private Double goodwill;
  private Double intangibleAssets;
  private Double goodwillAndIntangibleAssets;
  private Double longTermInvestments;
  private Double taxAssets;
  private Double otherNonCurrentAssets;
  private Double totalNonCurrentAssets;
  private Double otherAssets;
  private Double totalAssets;
  private Double accountPayables;
  private Double shortTermDebt;
  private Double taxPayables;
  private Double deferredRevenue;
  private Double otherCurrentLiabilities;
  private Double totalCurrentLiabilities;
  private Double longTermDebt;
  private Double deferredRevenueNonCurrent;
  private Double deferredTaxLiabilitiesNonCurrent;
  private Double otherNonCurrentLiabilities;
  private Double totalNonCurrentLiabilities;
  private Double otherLiabilities;
  private Double totalLiabilities;
  private Long commonStock;
  private Double retainedEarnings;
  private Double accumulatedOtherComprehensiveIncomeLoss;
  private Double othertotalStockholdersEquity;
  private Double totalStockholdersEquity;
  private Double totalLiabilitiesAndStockholdersEquity;
  private Double totalInvestments;
  private Double totalDebt;
  private Double netDebt;

  @Tolerate
  public BalanceSheetEntity() {

  }
}
