package org.toxicaker.models.entities;

import dev.morphia.annotations.Entity;
import dev.morphia.annotations.Id;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import org.bson.types.ObjectId;

@Entity("prices")
@Data
@Builder
public class PriceEntity {

  @Id
  private ObjectId id;
  private String symbol;
  private String date;
  private Double open;
  private Double high;
  private Double low;
  private Double close;
  private Long volume;
  private Double change;
  private Double changePercent;

  @Tolerate
  public PriceEntity() {
  }
}
