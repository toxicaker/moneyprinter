package org.toxicaker.models.entities;

import dev.morphia.annotations.Entity;
import dev.morphia.annotations.Id;
import dev.morphia.annotations.Property;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import org.bson.types.ObjectId;

@Entity("fund_holds")
@Data
@Builder
public class ArkHoldEntity {

  @Id
  private ObjectId id;
  private String symbol;
  private String company;
  private String date;
  @Property("target_symbol")
  private String targetSymbol;
  @Property("market_value")
  private Double marketValue;
  private Integer shares;
  private Double weight;

  @Tolerate
  public ArkHoldEntity() {

  }
}
