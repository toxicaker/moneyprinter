package org.toxicaker.models.entities;

import dev.morphia.annotations.Entity;
import dev.morphia.annotations.Id;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import org.bson.types.ObjectId;

@Entity("news")
@Data
@Builder
public class NewsEntity {

  @Id
  private ObjectId id;
  private String symbol;
  private String publishedDate;
  private String title;
  private String image;
  private String site;
  private String text;
  private String url;

  @Tolerate
  public NewsEntity() {
  }
}
