package org.toxicaker.models.entities;

import dev.morphia.annotations.Entity;
import dev.morphia.annotations.Id;
import dev.morphia.annotations.Property;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import org.bson.types.ObjectId;

@Entity("symbols")
@Data
@Builder
public class SymbolEntity {

  @Id
  private ObjectId id;
  private String symbol;
  private String name;
  private String exchange;
  @Property("financial_report_available")
  private Boolean financialStatementAvailable;
  private String type;

  @Tolerate
  public SymbolEntity() {

  }

  public enum Type {
    ETF("ETF"),
    COMMON_STOCK("COMMON_STOCK"),
    INDEX("INDEX"),
    MUTUAL_FUND("MUTUAL_FUND");
    public String val;

    Type(String val) {
      this.val = val;
    }
  }
}
