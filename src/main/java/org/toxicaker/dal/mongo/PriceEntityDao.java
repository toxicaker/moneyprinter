package org.toxicaker.dal.mongo;

import dev.morphia.AdvancedDatastore;
import dev.morphia.query.Sort;
import java.util.ArrayList;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.toxicaker.models.entities.PriceEntity;
import org.toxicaker.utils.MongoUtil;

@Component
@Slf4j
public class PriceEntityDao {

  /**
   * Save or update priceEntity based on date. For example, if date is 2021-01-01, the entity will
   * be saved to prices_2021 collection. This method doesn't override existing record
   */
  public String saveOrUpdatePriceEntity(PriceEntity priceEntity) {
    var date = priceEntity.getDate();
    var strs = date.split("-");
    AdvancedDatastore datastore = (AdvancedDatastore) MongoUtil.getDataStore();
    var existingEntity = getPriceEntityBySymbolAndDate(priceEntity.getSymbol(), date);
    if (existingEntity != null) {
      priceEntity.setId(existingEntity.getId());
    }
    var rst = datastore.save("prices" + strs[0], priceEntity);
    return rst.getId().toString();
  }

  /**
   * Find the PriceEntity by symbol and date
   *
   * @return null if does not exist
   */
  public PriceEntity getPriceEntityBySymbolAndDate(String symbol, String date) {
    if (StringUtils.isEmpty(date) || !date.contains("-")) {
      return null;
    }
    var year = date.split("-")[0];
    var coll = "prices" + year;
    AdvancedDatastore datastore = (AdvancedDatastore) MongoUtil.getDataStore();
    return datastore.createQuery(coll, PriceEntity.class)
        .field("symbol").equal(symbol)
        .field("date").equal(date)
        .first();
  }

  /**
   * List PriceEntities ordered by date ascending
   */
  public List<PriceEntity> listPriceEntities(String symbol, String from, String to) {
    AdvancedDatastore datastore = (AdvancedDatastore) MongoUtil.getDataStore();
    var rst = new ArrayList<PriceEntity>();
    var y1 = Integer.parseInt(from.split("-")[0]);
    var y2 = Integer.parseInt(to.split("-")[0]);

    for (int i = y1; i <= y2; i++) {
      var coll = "prices" + i;
      var priceEntities = datastore.createQuery(coll, PriceEntity.class)
          .field("symbol").equal(symbol)
          .field("date").greaterThanOrEq(from)
          .field("date").lessThanOrEq(to)
          .order(Sort.ascending("date")).find().toList();
      rst.addAll(priceEntities);
    }
    return rst;
  }

  public PriceEntity getLatestPriceEntityUnderDate(String symbol, String date) {
    var year = date.split("-")[0];
    var coll = "prices" + year;
    AdvancedDatastore datastore = (AdvancedDatastore) MongoUtil.getDataStore();
    var rst = datastore.createQuery(coll, PriceEntity.class)
        .field("symbol").equal(symbol)
        .field("date").lessThan(date)
        .order(Sort.descending("date"))
        .first();
    if (rst == null) {
      var lastYear = Integer.parseInt(date.split("-")[0]) - 1;
      coll = "prices" + lastYear;
      rst = datastore.createQuery(coll, PriceEntity.class)
          .field("symbol").equal(symbol)
          .field("date").lessThan(date)
          .order(Sort.descending("date"))
          .first();
    }
    return rst;
  }

  /**
   * Delete all the priceEntities by date
   */
  public void deletePriceEntitiesDate(String date) {
    var year = date.split("-")[0];
    var coll = "prices" + year;
    AdvancedDatastore datastore = (AdvancedDatastore) MongoUtil.getDataStore();
    var query = datastore.createQuery(coll, PriceEntity.class).field("date").equal(date);
    datastore.delete(query);
  }
}
