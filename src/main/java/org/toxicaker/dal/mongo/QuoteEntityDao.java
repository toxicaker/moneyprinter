package org.toxicaker.dal.mongo;

import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.toxicaker.models.entities.QuoteEntity;
import org.toxicaker.utils.MongoUtil;

@Component
@Slf4j
public class QuoteEntityDao {

  public QuoteEntity getQuoteEntityBySymbol(String symbol) {
    return MongoUtil.getDataStore()
        .createQuery(QuoteEntity.class)
        .field("symbol").equal(symbol)
        .first();
  }

  public String saveOrUpdateQuoteEntity(QuoteEntity quoteEntity) {
    var symbol = quoteEntity.getSymbol();
    var existingEntity = getQuoteEntityBySymbol(symbol);
    if (existingEntity != null) {
      quoteEntity.setId(existingEntity.getId());
    }
    MongoUtil.getDataStore().save(quoteEntity);
    return quoteEntity.getId().toHexString();
  }

  /**
   * Search stock symbols based on keyword
   */
  public List<String> searchSymbols(String keyword) {
    var pattern = Pattern.compile(".*" + keyword + ".*", Pattern.CASE_INSENSITIVE);
    return MongoUtil.getDataStore()
        .createQuery(QuoteEntity.class)
        .filter("symbol", pattern)
        .find()
        .toList()
        .stream()
        .map(QuoteEntity::getSymbol)
        .collect(Collectors.toList());
  }
}
