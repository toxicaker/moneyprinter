package org.toxicaker.dal.mongo;

import dev.morphia.query.Sort;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.toxicaker.models.entities.ArkHoldEntity;
import org.toxicaker.models.entities.ArkTradeEntity;
import org.toxicaker.utils.MongoUtil;

@Component
@Slf4j
public class ArkEntityDao {

  /**
   * List ARK fund trade operations
   *
   * @param symbol EFT symbol: ARKK, ARKW......
   * @param date   Eg: 2020-01-01
   */
  public List<ArkTradeEntity> listArkTradesBySymbolAndDate(String symbol, String date) {
    return MongoUtil.getDataStore().createQuery(ArkTradeEntity.class)
        .field("symbol").equal(symbol)
        .field("date").equal(date)
        .find().toList();
  }

  /**
   * List ARK fund trade operations for a specific target symbol
   *
   * @param symbol       EFT symbol: ARKK, ARKW......
   * @param targetSymbol Company symbol
   * @param from         Start date
   * @param to           End date
   */
  public List<ArkTradeEntity> listArkTradesBySymbolAndTargetSymbol(String symbol,
      String targetSymbol,
      String from, String to) {
    return MongoUtil.getDataStore().createQuery(ArkTradeEntity.class)
        .order(Sort.ascending("date"))
        .field("symbol").equal(symbol)
        .field("target_symbol").equal(targetSymbol)
        .field("date").greaterThanOrEq(from)
        .field("date").lessThanOrEq(to)
        .find().toList();
  }

  /**
   * List ARK fund stock holding
   *
   * @param symbol EFT symbol: ARKK, ARKW......
   * @param date   Eg: 2020-01-01
   */
  public List<ArkHoldEntity> listArkHoldsBySymbolAndDate(String symbol, String date) {
    return MongoUtil.getDataStore().createQuery(ArkHoldEntity.class)
        .order(Sort.descending("weight"))
        .field("symbol").equal(symbol)
        .field("date").equal(date)
        .find().toList();
  }

  /**
   * Get ArkHoldEntity for a specific company symbol
   *
   * @param symbol       EFT symbol: ARKK, ARKW......
   * @param targetSymbol Company symbol
   * @param date         Eg: 2020-01-01
   */
  public ArkHoldEntity getArkHoldBySymbolAndTargetSymbolAndDate(String symbol, String targetSymbol,
      String date) {
    return MongoUtil.getDataStore().createQuery(ArkHoldEntity.class)
        .field("symbol").equal(symbol)
        .field("target_symbol").equal(targetSymbol)
        .field("date").equal(date)
        .find().tryNext();
  }

  /**
   * Get ArkTradeEntity for a specific company symbol
   *
   * @param symbol       EFT symbol: ARKK, ARKW......
   * @param targetSymbol Company symbol
   * @param date         Eg: 2020-01-01
   * @param shares       share number
   */
  public ArkTradeEntity getArkTradeBySymbolAndTargetSymbolAndDateAndShares(String symbol,
      String targetSymbol, String date, int shares) {
    return MongoUtil.getDataStore().createQuery(ArkTradeEntity.class)
        .field("symbol").equal(symbol)
        .field("target_symbol").equal(targetSymbol)
        .field("date").equal(date)
        .field("shares").equal(shares)
        .find().tryNext();
  }


  /**
   * List ARK fund stock holding information for a specific target symbol
   *
   * @param symbol       EFT symbol: ARKK, ARKW......
   * @param targetSymbol Company symbol
   * @param from         Start date
   * @param to           End date
   */
  public List<ArkHoldEntity> listArkHoldsBySymbolAndTargetSymbol(String symbol, String targetSymbol,
      String from, String to) {
    return MongoUtil.getDataStore().createQuery(ArkHoldEntity.class)
        .order(Sort.ascending("date"))
        .field("symbol").equal(symbol)
        .field("target_symbol").equal(targetSymbol)
        .field("date").greaterThanOrEq(from)
        .field("date").lessThanOrEq(to)
        .find().toList();
  }

  /**
   * List available dates of ArkHoldEntity saved in the MongoDB
   *
   * @return List of date string
   */
  public List<String> listArkHoldsAvailableDate() {
    var rst = MongoUtil.getDataStore().getCollection(ArkHoldEntity.class).distinct("date");
    List<String> dates = new ArrayList<>();
    for (Object o : rst) {
      dates.add(o.toString());
    }
    Collections.sort(dates);
    return dates;
  }

  /**
   * List available dates of ArkTradeEntity saved in the MongoDB
   *
   * @return List of date string, ascending order
   */
  public List<String> listArkTradesAvailableDate() {
    var rst = MongoUtil.getDataStore().getCollection(ArkTradeEntity.class).distinct("date");
    List<String> dates = new ArrayList<>();
    for (Object o : rst) {
      dates.add(o.toString());
    }
    Collections.sort(dates);
    return dates;
  }

  /**
   * Get the latest ARK trading date based on ArkHold before the current date.
   *
   * @param date Eg: 2020-01-01
   * @return null If doesn't exist
   */
  public String getArkHoldLatestDateBefore(String date) {
    try {
      return MongoUtil.getDataStore()
          .createQuery(ArkHoldEntity.class)
          .order(Sort.descending("date"))
          .field("date")
          .lessThan(date)
          .find()
          .next()
          .getDate();
    } catch (NoSuchElementException ex) {
      return null;
    }
  }

  /**
   * Save ArkHoldEntity if the database doesn't have the record
   *
   * @return Id
   */
  public String saveArkHoldIfNotExist(ArkHoldEntity arkHoldEntity) {
    var entity = getArkHoldBySymbolAndTargetSymbolAndDate(
        arkHoldEntity.getSymbol(), arkHoldEntity.getTargetSymbol(), arkHoldEntity.getDate());
    if (entity == null) {
      var rst = MongoUtil.getDataStore().save(arkHoldEntity);
      return rst.getId().toString();
    }
    return entity.getId().toHexString();
  }

  /**
   * Save ArkTradeEntity if the database doesn't have the record
   *
   * @return Id
   */
  public String saveArkTradeIfNotExist(ArkTradeEntity arkTradeEntity) {
    var entity = getArkTradeBySymbolAndTargetSymbolAndDateAndShares(
        arkTradeEntity.getSymbol(), arkTradeEntity.getTargetSymbol(),
        arkTradeEntity.getDate(), arkTradeEntity.getShares());

    if (entity == null) {
      var rst = MongoUtil.getDataStore().save(arkTradeEntity);
      return rst.getId().toString();
    }
    return entity.getId().toHexString();
  }
}
