package org.toxicaker.dal.mongo;

import dev.morphia.query.Sort;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.toxicaker.models.entities.BalanceSheetEntity;
import org.toxicaker.models.entities.CashFlowEntity;
import org.toxicaker.models.entities.CompanyProfileEntity;
import org.toxicaker.models.entities.IncomeStatementEntity;
import org.toxicaker.models.entities.NewsEntity;
import org.toxicaker.utils.MongoUtil;

@Component
@Slf4j
public class CompanyDao {

  /**
   * Save company profile. If exists, will update the record.
   */
  public String saveOrUpdateCompanyProfileEntity(CompanyProfileEntity companyProfileEntity) {
    var symbol = companyProfileEntity.getSymbol();
    var existingEntity = getCompanyProfileEntityBySymbol(symbol);
    if (existingEntity != null) {
      companyProfileEntity.setId(existingEntity.getId());
    }
    MongoUtil.getDataStore().save(companyProfileEntity);
    return companyProfileEntity.getId().toHexString();
  }

  /**
   * Find CompanyProfileEntity by symbol
   *
   * @return null if not exits
   */
  public CompanyProfileEntity getCompanyProfileEntityBySymbol(String symbol) {
    return MongoUtil.getDataStore()
        .createQuery(CompanyProfileEntity.class)
        .field("symbol")
        .equal(symbol)
        .find()
        .tryNext();
  }

  /**
   * List CompanyProfileEntities by industry
   */
  public List<CompanyProfileEntity> listCompanyProfileEntitiesByIndustry(String industry) {
    if (StringUtils.isEmpty(industry)) {
      return new ArrayList<>();
    }
    return MongoUtil.getDataStore()
        .createQuery(CompanyProfileEntity.class)
        .field("industry")
        .equal(industry)
        .find().toList();
  }

  public Set<String> listSectors() {
    var rst = MongoUtil.getDataStore().getCollection(CompanyProfileEntity.class).distinct("sector");
    Set<String> sectors = new HashSet<>();
    for (Object o : rst) {
      var s = o.toString();
      if (StringUtils.isEmpty(s) || "N/A".equals(s)) {
        continue;
      }
      sectors.add(s);
    }
    return sectors;
  }

  public Set<String> listIndustriesBySector(String sector) {
    var rst = MongoUtil.getDataStore()
        .createQuery(CompanyProfileEntity.class)
        .field("sector")
        .equal(sector)
        .find().toList();
    var industries = new HashSet<String>();
    for (var i : rst) {
      if (StringUtils.isEmpty(i.getIndustry()) || "N/A".equals(i.getIndustry())) {
        continue;
      }
      industries.add(i.getIndustry());
    }
    return industries;
  }

  /**
   * Search news by title
   *
   * @return null if not exists
   */
  public NewsEntity getNewsEntityByTitle(String title) {
    return MongoUtil.getDataStore()
        .createQuery(NewsEntity.class)
        .field("title")
        .equal(title)
        .find()
        .tryNext();
  }

  /**
   * Save the NewsEntity only if it doesn't exist
   */
  public String saveNewsEntityIfNotExists(NewsEntity newsEntity) {
    var existingEntity = getNewsEntityByTitle(newsEntity.getTitle());
    if (existingEntity == null) {
      MongoUtil.getDataStore().save(newsEntity);
    }
    return newsEntity.getId().toHexString();
  }

  /**
   * List the news ordered by date
   *
   * @param limit the maximum number returned by the function
   */
  public List<NewsEntity> listNewsEntitiesBySymbolOrderedByDateDesc(String symbol, int limit) {
    return MongoUtil.getDataStore()
        .createQuery(NewsEntity.class)
        .field("symbol")
        .equal(symbol)
        .order(Sort.descending("publishedDate"))
        .limit(limit)
        .find().toList();
  }

  /**
   * Search income statement by date
   *
   * @return null if not exists
   */
  public IncomeStatementEntity getIncomeStatementEntityBySymbolAndDateAndPeriod(String symbol,
      String date, String period) {
    return MongoUtil.getDataStore()
        .createQuery(IncomeStatementEntity.class)
        .field("date")
        .equal(date)
        .field("symbol")
        .equal(symbol)
        .field("period")
        .equal(period)
        .find()
        .tryNext();
  }

  /**
   * List all income statements by symbol
   * @param quarterly = true: list Q1, Q2, Q3, Q4, false: FY
   */
  public List<IncomeStatementEntity> listIncomeStatementEntitiesBySymbol(String symbol,
      boolean quarterly) {
    if (quarterly) {
      return MongoUtil.getDataStore()
          .createQuery(IncomeStatementEntity.class)
          .field("symbol")
          .equal(symbol)
          .field("period")
          .notEqual("FY")
          .find().toList();
    } else {
      return MongoUtil.getDataStore()
          .createQuery(IncomeStatementEntity.class)
          .field("symbol")
          .equal(symbol)
          .field("period")
          .equal("FY")
          .find().toList();
    }
  }

  /**
   * Save the IncomeStatementEntity only if it doesn't exist
   */
  public String saveIncomeStatementEntityIfNotExists(IncomeStatementEntity incomeStatementEntity) {
    var existingEntity = getIncomeStatementEntityBySymbolAndDateAndPeriod(
        incomeStatementEntity.getSymbol(),
        incomeStatementEntity.getDate(), incomeStatementEntity.getPeriod());
    if (existingEntity == null) {
      MongoUtil.getDataStore().save(incomeStatementEntity);
    }
    return incomeStatementEntity.getId().toHexString();
  }

  /**
   * Search balance sheet by date
   *
   * @return null if not exists
   */
  public BalanceSheetEntity getBalanceSheetEntityBySymbolAndDateAndPeriod(String symbol,
      String date, String period) {
    return MongoUtil.getDataStore()
        .createQuery(BalanceSheetEntity.class)
        .field("date")
        .equal(date)
        .field("symbol")
        .equal(symbol)
        .field("period")
        .equal(period)
        .find()
        .tryNext();
  }

  /**
   * List all balance sheets by symbol
   * @param quarterly = true: list Q1, Q2, Q3, Q4, false: FY
   */
  public List<BalanceSheetEntity> listBalanceSheetEntitiesBySymbol(String symbol,
      boolean quarterly) {
    if (quarterly) {
      return MongoUtil.getDataStore()
          .createQuery(BalanceSheetEntity.class)
          .field("symbol")
          .equal(symbol)
          .field("period")
          .notEqual("FY")
          .find().toList();
    } else {
      return MongoUtil.getDataStore()
          .createQuery(BalanceSheetEntity.class)
          .field("symbol")
          .equal(symbol)
          .field("period")
          .equal("FY")
          .find().toList();
    }
  }

  /**
   * Save the BalanceSheetEntity only if it doesn't exist
   */
  public String saveBalanceSheetEntityIfNotExists(BalanceSheetEntity balanceSheetEntity) {
    var existingEntity = getBalanceSheetEntityBySymbolAndDateAndPeriod(
        balanceSheetEntity.getSymbol(),
        balanceSheetEntity.getDate(), balanceSheetEntity.getPeriod());
    if (existingEntity == null) {
      MongoUtil.getDataStore().save(balanceSheetEntity);
    }
    return balanceSheetEntity.getId().toHexString();
  }

  /**
   * Search cash flow by date
   *
   * @return null if not exists
   */
  public CashFlowEntity getCashFLowEntityBySymbolAndDateAndPeriod(String symbol, String date,
      String period) {
    return MongoUtil.getDataStore()
        .createQuery(CashFlowEntity.class)
        .field("date")
        .equal(date)
        .field("symbol")
        .equal(symbol)
        .field("period")
        .equal(period)
        .find()
        .tryNext();
  }

  /**
   * List all cash flow statements by symbol
   * @param quarterly = true: list Q1, Q2, Q3, Q4, false: FY
   */
  public List<CashFlowEntity> listCashFlowEntitiesBySymbol(String symbol,
      boolean quarterly) {
    if (quarterly) {
      return MongoUtil.getDataStore()
          .createQuery(CashFlowEntity.class)
          .field("symbol")
          .equal(symbol)
          .field("period")
          .notEqual("FY")
          .find().toList();
    } else {
      return MongoUtil.getDataStore()
          .createQuery(CashFlowEntity.class)
          .field("symbol")
          .equal(symbol)
          .field("period")
          .equal("FY")
          .find().toList();
    }
  }

  /**
   * Save the CashFlowEntity only if it doesn't exist
   */
  public String saveCashFlowEntityIfNotExists(CashFlowEntity cashFlowEntity) {
    var existingEntity = getCashFLowEntityBySymbolAndDateAndPeriod(cashFlowEntity.getSymbol(),
        cashFlowEntity.getDate(), cashFlowEntity.getPeriod());
    if (existingEntity == null) {
      MongoUtil.getDataStore().save(cashFlowEntity);
    }
    return cashFlowEntity.getId().toHexString();
  }
}
